import Logout from 'views/Logout'

import Profile from 'views/UserProfile/Profile'
import Icons from 'views/UserProfile/Icons'


var navRoutes = [
{
    path: '/user_profile',
    name: 'Profil',
    icon: " ni ni-single-02 text-blue",
    component: Profile,
    layout: '/admin'
},
{
    path: '/icons',
    name: 'Ikonki',
    icon: " ni ni-archive-2 text-orange",
    component: Icons,
    layout: '/admin'
},
{
    path: '/logout',
    name: 'Wyloguj',
    icon: " ni ni-archive-2 text-red",
    component: Logout,
    layout: '/admin'
  },


];

export default navRoutes;