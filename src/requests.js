import Api from './api';
import Swal from 'sweetalert2'

export const destroy = (str, callback) => {
  Swal.fire({
    title: 'Potwierdź usunięcie',
    text: "Tej operacji nie da się cofnąć!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Usuń!',
    cancelButtonText: "Anuluj",
    reverseButtons: true
  }).then(async result => {
    if (result.value) {
      const res = await Api().delete(str)
      if (res.status === 204) {
        Swal.fire({
          title: 'Usunięto!',
          text: 'Rekord został usunięty.',
          icon: 'success',
          timer: 1000,
          showConfirmButton: false,
          timerProgressBar: true
        })
        callback()
      } else {
        Swal.fire({
          title: 'Błąd podczas usuwania',
          text: 'Spróbuj ponownie',
          icon: 'error',
          timer: 1000,
          showConfirmButton: false,
          timerProgressBar: true
        })
      }
    }
  })
}
