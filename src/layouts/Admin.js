import React, { useEffect } from "react"
import { Route, Switch, Redirect } from "react-router-dom"
import { Container } from "reactstrap"
import AdminNavbar from "components/Navbars/AdminNavbar.js"
import AdminFooter from "components/Footers/AdminFooter.js"
import Sidebar from "components/Sidebar/Sidebar.js"
import { connect } from "react-redux"
import { currentInstitution } from '../utils'

import routes from "routes.js";
import navRoutes from 'navRoutes'

const Admin = (props) => {
  React.useEffect(() => {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
  }, [])

  const getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.layout === "/admin") {
        return (
          <Route
            path={`/${currentInstitution()}` + prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  }
  
  return (
    <>
      <Sidebar
        {...props}
        routes={routes}
        logo={{
          innerLink: "/admin/index",
          imgSrc: require("assets/img/brand/argon-react.png"),
          imgAlt: "..."
        }}
        />
      <div className="main-content">
        <AdminNavbar
          {...props}
          routes={navRoutes}
          logo={{
            innerLink: "/admin/index",
            imgSrc: require("assets/img/brand/argon-react.png"),
            imgAlt: "..."
          }}
        />
        <Switch>
          <Redirect 
            from={`/${currentInstitution()}/admin/index`}
            to={`/${currentInstitution()}/admin/users`}
          />
          {getRoutes(routes)}
          <Redirect from="*" to={`/${currentInstitution()}/admin/users`} />
        </Switch>
        <Container fluid>
          <AdminFooter />
        </Container>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  role: state.auth.role,
  institution: state.auth.institution
})

export default connect(mapStateToProps)(Admin);
