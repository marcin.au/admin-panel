import React from 'react'
import { createBrowserHistory } from 'history' 
import { Router, Switch, Route, Redirect } from 'react-router'
import { Provider, useSelector } from 'react-redux'
import store from './redux/store'
import { currentInstitution } from './utils'

import "assets/plugins/nucleo/css/nucleo.css"
import "@fortawesome/fontawesome-free/css/all.min.css"
import "assets/scss/argon-dashboard-react.scss"
import './styles/App.css'

import AdminLayout from "layouts/Admin.js"
import AuthLayout from "layouts/Auth.js"
import Initializer from './Initializer'

const history = createBrowserHistory()

const App = () => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Initializer />
        <Routes />
      </Router>
    </Provider>
  )
}


const Routes = () => {
  // Need it to refresh routes after institution change
  useSelector(state => state.auth.institution)

  return (
    <Switch>
      <Route path={`/${currentInstitution()}/admin`} render={props => <AdminLayout {...props} />} />
      <Route path={`/${currentInstitution()}`} render={props => <AuthLayout {...props} />} />
      
      {/* Need it to refresh routes after institution change */}
      <Redirect from="*" to={document.location.pathname}/>
    </Switch>
  )
}

export default App
