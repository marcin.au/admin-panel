import Axios from 'axios'
import store from './redux/store'
import Swal from 'sweetalert2'
import { currentInstitution } from './utils'

export const url = process.env.REACT_APP_API_URL

// contentType = 'application/json'

const axios = (config) => {
  const institutionId = store.getState().auth.institution?.slug
  const instance = Axios.create({
    baseURL: config?.withInstitution ? `${url}/institutions/${institutionId}` : url,
    timeout: 10000,
    headers: {
      // 'Content-type': contentType,
      'Authorization': store.getState().auth.token
    }
  })
  instance.interceptors.response.use(res => handle(res), err => {
    if (err.response) return handle(err.response)
    Promise.reject({error: err}).catch(_ => _)
    return {status: 0, data: []}
  })
  return instance
}

export const login = async (email, password) => {
  const payload = { user: { email, password } }
  try {
    const res = await Axios.post(`${url}/sessions`, payload)
    if (res.status === 201) {
      console.log(res)
      localStorage.setItem('token', res.headers.authorization)
      return true
    }
    return false
  }
  catch (err) {
    console.log(err.response)
    return false
  }
}

const handlers = {
  0: () => {
    console.log('Connection error')
  },
  401: () => {
    console.log('Unauthorized')
    window.location.replace(`/${currentInstitution()}/login`)
    Swal.fire({
      icon: 'warning',
      title: 'Wylogowano',
      showConfirmButton: false,
      timerProgressBar: true,
      timer: 1500
    })
  },
  403: () => {
    console.log('Forbitten')
  },
  500: () => {
    console.log('Server error')
  }
}

const handle = res => {
  if (handlers[res.status]) {
    handlers[res.status](res)
    return { ...res, data: [] }
  }
  return res
}

export default axios
