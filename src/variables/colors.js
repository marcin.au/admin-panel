export default {
  primary: '#5e72e4',
  light: '#11cdef',
  grey: '#f7fafc'
}