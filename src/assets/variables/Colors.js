export default {
  red: '#FF4136',
  green: '#2ECC40',
  blue: '#0074D9',
  yellow: '#FFDC00',
  orange: '#FF851B',
  grey: '#999'
}