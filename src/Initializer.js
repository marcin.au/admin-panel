import { useEffect } from 'react'
import { withRouter } from 'react-router'
import { useSelector, useDispatch } from 'react-redux'
import { SET_TOKEN, SET_ID } from 'redux/actions/auth'
import { fillUserInfo } from 'redux/actionCreators/auth'
import { currentInstitution } from './utils'

const Initializer = (props) => {
  const loggedIn = useSelector(state => state.auth.loggedIn)
  const dispatch = useDispatch()

  const init = async () => {
    const id = localStorage.getItem('id')
    const token = localStorage.getItem('token')

    if (id && token) {
      dispatch({ type: SET_TOKEN, token })
      dispatch({ type: SET_ID, id })
      try {
        await dispatch(fillUserInfo(id, token))
      }
      catch (err) {
        return
      }
    }
  }

  useEffect(() => {
    init()
  }, [])
  
  useEffect(() => {
    const path = window.location.pathname
    const token = localStorage.getItem('token')
    if (path === '/' && !loggedIn && token) {
      return
    }
    if (path === '/' && !loggedIn && !token) {
      props.history.push(`/${currentInstitution()}/login`)
    }
    if (path === '/' && loggedIn) {
      props.history.push(`/${currentInstitution()}/admin/institutions`)
    }
    if (path === '/login' && loggedIn) {
      props.history.push(`/${currentInstitution()}/admin/institutions`)
    }
  }, [loggedIn, props.history])

  return null
}

export default withRouter(Initializer)
