import React from 'react'
import {UncontrolledDropdown, DropdownToggle, DropdownItem, DropdownMenu} from 'reactstrap'

const Actions = props => {
  return (
      <UncontrolledDropdown>
        <DropdownToggle
          className="btn-icon-only text-light"
          href="#pablo"
          role="button"
          size="sm"
          color=""
          onClick={e => e.preventDefault()}
        >
          <i className="fas fa-ellipsis-v" />
        </DropdownToggle>
        <DropdownMenu className="dropdown-menu-arrow" right>
          <DropdownItem
            onClick={e => {e.preventDefault(); props.showHandler()}}
            href='#'
          >
            Pokaż
          </DropdownItem>
          <DropdownItem
            href="#pablo"
            onClick={e => {e.preventDefault(); props.editHandler()}}
          >
            Edytuj
          </DropdownItem>
          <DropdownItem
            href="#pablo"
            onClick={e => {e.preventDefault(); props.deleteHandler()}}
          >
            Usuń
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
  )
}

export default Actions