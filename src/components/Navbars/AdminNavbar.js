import React from "react";
import { Link } from "react-router-dom";
// reactstrap components
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Navbar,
  Nav,
  Container,
  Media
} from "reactstrap";
import { useSelector} from 'react-redux'
import { PropTypes } from "prop-types";
import InstitutionSelect from '../InstitutionSelect'
import StateManager from "react-select";

const AdminNavbar = props => {

  const user = useSelector(store => store.auth.userData)
  const role = useSelector(state => state.auth.role)
  const institution = useSelector(state => state.auth.institution)

  const { routes, logo } = props;
    let navbarBrandProps;
    if (logo && logo.innerLink) {
      navbarBrandProps = {
        to: logo.innerLink,
        tag: Link
      };
    } else if (logo && logo.outterLink) {
      navbarBrandProps = {
        href: logo.outterLink,
        target: "_blank"
      };
    }

  const createLinks = routes => {
    return routes.filter(route => route.display !== false).map((prop, key) => {
      return (
        <DropdownItem key={key} to={prop.layout + prop.path} tag={Link}>
          <i className={prop.icon} />
          <span>{prop.name}</span>
        </DropdownItem>
      );
    });
  };

  const link = (
    <span className="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block">{institution.name ?? institution.slug}</span>
  )

    return (
      <>
        <Navbar className="navbar-top navbar-dark" expand="md" id="navbar-main">
          <Container fluid>
            {role === 'superadmin' ? <div /> : link}
            <Nav className="align-items-center d-none d-md-flex" navbar>
              {role === 'superadmin' ? <InstitutionSelect /> : null}
              <UncontrolledDropdown nav>
                <DropdownToggle className="pr-0" nav>
                  <Media className="align-items-center">
                    <span className="avatar avatar-sm rounded-circle">
                      <img
                        alt="..."
                        src={require("assets/img/theme/team-4-800x800.jpg")}
                      />
                    </span>
                    <Media className="ml-2 d-none d-lg-block">
                      <span className="mb-0 text-sm font-weight-bold">
                        {user.first_name}
                      </span>
                    </Media>
                  </Media>
                </DropdownToggle>
                <DropdownMenu className="dropdown-menu-arrow" right>
                  {createLinks(routes)}
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Container>
        </Navbar>
      </>
    );
}

AdminNavbar.defaultProps = {
  routes: [{}]
};

AdminNavbar.propTypes = {
  // links that will be displayed inside the component
  routes: PropTypes.arrayOf(PropTypes.object),
  logo: PropTypes.shape({
    // innerLink is for links that will direct the user within the app
    // it will be rendered as <Link to="...">...</Link> tag
    innerLink: PropTypes.string,
    // outterLink is for links that will direct the user outside the app
    // it will be rendered as simple <a href="...">...</a> tag
    outterLink: PropTypes.string,
    // the image src of the logo
    imgSrc: PropTypes.string.isRequired,
    // the alt for the img
    imgAlt: PropTypes.string.isRequired
  })
}


export default AdminNavbar;
