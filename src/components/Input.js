import React from 'react'
import { FormGroup, Input } from 'reactstrap'
import Proptypes from 'prop-types'
import Colors from '../assets/variables/Colors'

const MyInput = props => {

  const field = props.data

  let classes = "form-control"
  if (field.touched && !field.errors) {
    classes += ' is-valid'
  }
  if (field.errors) {
    classes += ' is-invalid'
  }

  const errorStyle = {
    color: Colors.red,
    display: 'block',
    fontSize: '12px'
  }

  let errors = null
  if (field.errors) {
    errors = field.errors.map((item, index) => (
      <span style={errorStyle} key={index}>✘ {item}</span>
    ))
  }

  const optionalStyle = {
    color: 'grey',
    fontSize: '12px',
    marginLeft: '10px'
  }

  const optional = <span style={optionalStyle}>(Opcjonalnie)</span>

  return (
    <FormGroup>
      <label className="form-control-label">
        {props.title}{field.required ? null : optional}
      </label>
      <Input
        className={classes}
        placeholder={props.placeholder ? props.placeholder : ''}
        value={props.data.value ? props.data.value : ''}
        name={props.name}
        onChange={props.onChange}
        type={props.type ? props.type : 'text'}
        disabled={props.disabled}
        defaultValue={props.defaultValue}
        rows={props.rows}
        min={props.min}
      >
        {props.children}
      </Input>
      <div style={{marginTop: '10px', display: errors ? 'block' : 'none'}}>
        {errors}
      </div>
    </FormGroup>
  )
}

MyInput.propTypes = {
  data: Proptypes.object.isRequired,
  onChange: Proptypes.func,
  title: Proptypes.string.isRequired,
  name: Proptypes.string.isRequired,
  type: Proptypes.string,
  placeholder: Proptypes.string,
  disabled: Proptypes.bool,
  defaultValue: Proptypes.string,
  rows: Proptypes.string,
  min: Proptypes.string
}

export default MyInput
