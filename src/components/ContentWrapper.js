import React from 'react'
import { Container, Row, Card, CardHeader, Button } from 'reactstrap'
import Header from './Headers/Header'
import { useHistory } from 'react-router'

const ContentWrapper = props => {
  const history = useHistory()

  return (
    <span className='content-wrapper'>
      <Header />
      <Container className="mt--7" fluid>
        <Row>
          <div className="col">
            <Card className="bg-secondary shadow">
              <CardHeader className="border-0 content-wrapper-header">
                <h3 className="mb-0">{props.title}</h3>
                <Button color='primary' onClick={() => history.goBack()}>Cofnij</Button>
              </CardHeader>
              {props.children}
            </Card>
          </div>
        </Row>
      </Container>
    </span>
  )
}

export default ContentWrapper
