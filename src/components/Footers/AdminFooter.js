import React from "react";
import { Row, Col } from "reactstrap";

class Footer extends React.Component {
  render() {
    return (
      <footer className="footer">
        <Row className="align-items-center justify-content-xl-between">
          <Col xl="10"></Col>
          <Col xl="2">
          <div className="copyright text-right text-xl-right text-muted">
              © 2020{" "}
              <a
                className="font-weight-bold ml-1"
                href="http://www.media-park.pl/"
                rel="noopener noreferrer"
                target="_blank"
              >
                MediaPark
              </a>
            </div>
          </Col>
        </Row>
      </footer>
    );
  }
}

export default Footer;
