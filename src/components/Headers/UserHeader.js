import React from "react";

// reactstrap components
import { Button, Container, Row, Col } from "reactstrap";
import { withRouter } from 'react-router'


const UserHeader = props => {
    return (
      <>
        <div className="header bg-gradient-info pb-8 pt-5 pt-md-8">
          {/* Mask */}
          {/* <span className="mask bg-gradient-default opacity-8" /> */}
          {/* Header container */}
          <Container className="d-flex align-items-center" fluid>
            {/* <Row>
              <Col lg="7" md="10">
                <h1 className="display-2 text-white">Hello Jesse</h1>
                <p className="text-white mt-0 mb-5">
                  This is your profile page. You can see the progress you've
                  made with your work and manage your projects or assigned tasks
                </p> */}
                <Button
                  color="primary"
                  onClick={props.onClick}
                >
                  Edytuj profil
                </Button>
              {/* </Col>
            </Row> */}
          </Container>
        </div>
      </>
    );
  }

export default withRouter(UserHeader);
