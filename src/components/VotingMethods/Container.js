import React from 'react'
import PropTypes from 'prop-types'
import VotingSelect from './Select'
import { Col, UncontrolledAlert } from 'reactstrap'
import PeselList from './PeselList'

const styles = {
  alert: {
    height: 45,
    paddingTop: 7,
    marginBottom: 0,
    marginTop: 30
  }
}

const Container = (props) => {
  const peselList = (
    <PeselList consultationId={props.consultationId} initialCount={props.initialCount}/>
  )

  const alert = (
    <UncontrolledAlert color="info" style={styles.alert}>
      <span className="alert-icon">
        <i className="ni ni-like-2" />
      </span>
      <span className="alert-text ml-1">
        Dodawanie peseli możliwe jest po edycji konsultacji
      </span>
    </UncontrolledAlert>
  )

  const additionalContent = props.type === 'new' ? alert : peselList

  return (
    <React.Fragment>
      <Col lg="6">
        <VotingSelect value={props.value} onChange={props.onChange} />
      </Col>
      <Col lg="6">
        {props.value === 'pesel_whitelist' ? additionalContent : null}
      </Col>
    </React.Fragment>
  )
}

Container.defaultProps = {
  value: 'open',
  onChange: (e) => console.log(e),
  style: {},
  consultationId: null,
  type: 'new',
  initialCount: 0
}

Container.propTypes = {
  value: PropTypes.oneOf(['open', 'pesel', 'pesel_whitelist']),
  onChange: PropTypes.func,
  style: PropTypes.object,
  consultationId: PropTypes.string,
  type: PropTypes.oneOf(['new', 'edit']),
  initialCount: PropTypes.number
}

export default Container
