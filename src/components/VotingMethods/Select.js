import React from 'react'
import PropTypes from 'prop-types'
import { Input } from 'reactstrap'

const Select = (props) => {
  return (
    <div>
      <label className='form-control-label'>Wybierz metodę głosowania</label>
      <Input
        type="select"
        value={props.value}
        onChange={(event) => props.onChange(event.target.value)}
      >
        <option value="open">Otwarta</option>
        <option value="pesel">Pesel</option>
        <option value="pesel_whitelist">Wybrane Pesele</option>
      </Input>
    </div>
  )
}

Select.defaultProps = {
  value: 'open',
  onChange: (e) => console.log(e),
  style: {}
}

Select.propTypes = {
  value: PropTypes.oneOf(['open', 'pesel', 'pesel_whitelist']),
  onChange: PropTypes.func,
  style: PropTypes.object
}

export default Select
