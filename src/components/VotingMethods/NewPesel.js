import React from 'react'
import PropTypes from 'prop-types'
import { Input, Button, Spinner } from 'reactstrap'
import * as Requests from './Requests'
import Colors from '../../assets/variables/Colors'
import { validatePolish } from 'validate-polish'
import { notify } from './Alerts'

const styles = {
  container: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  input: {
    width: 200,
    display: 'inline-block',
    marginRight: 10
  },
  button: {
    width: 80
  },
  feedback: {
    display: 'inline-block',
    color: Colors.red,
    marginLeft: 10
  }
}

const NewPesel = (props) => {
  const [newPesel, setNewPesel] = React.useState('')
  const [feedBack, setFeedBack] = React.useState('')
  const [touched, setTouched] = React.useState(false)
  const [loading, setLoading] = React.useState(false)

  const validator = (pesel) => {
    if (pesel === '') return 'Pesel jest pusty' 
    if (pesel.length < 11) return 'Pesel jest za krótki'
    if (!validatePolish.pesel(pesel)) return 'Pesel jest nieprawidłowy'
    if (!props.isUnique(pesel)) return 'Pesel jest już na liście'
    return ''
  }

  React.useEffect(() => {
    if (!touched) return

    setFeedBack(validator(newPesel))
  }, [newPesel])

  const valid = feedBack === ''

  const createNewPesel = async () => {
    if (!valid || !touched) return

    setLoading(true)
    const id = await Requests.createWhiteListItem({
      data: newPesel,
      consultationId: props.consultationId
    })
    setLoading(false)
    setTouched(false)
    setNewPesel('')

    if (id === 'already-taken') {
      notify('Pesel był już wcześniej dodany', null, 'warning')
      return
    }
    if (!id) {
      notify('Błąd podczas dodawania numeru Pesel', null, 'error')
      return
    }

    props.onNewItem({ id, value: newPesel })
  }

  const keyHandler = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault()
      createNewPesel()
    }
  }

  const onChange = (event) => {
    if (event.target.value.length === 12) return

    setNewPesel(event.target.value)
    if (!touched) setTouched(true)
  }

  const loader = <Spinner size="sm"/>

  const addedText = props.count ? ` (Obecnie dodanych ${props.count})` : ''

  const deleteAll = (
    <Button
      color="danger"
      style={styles.delete}
      onClick={props.deleteAll}
    >
      Usuń wszystkie
    </Button>
  )

  return (
    <div>
      <label className='form-control-label'>
        Dodaj nowe numery pesel{addedText}
      </label>
      <div style={styles.container}>
        <div>
          <Input
            type="number"
            value={newPesel}
            onChange={onChange}
            className={touched ? (valid ? 'is-valid' : 'is-invalid') : null}
            placeholder="Nowy PESEL..."
            onKeyDown={keyHandler}
            style={styles.input}
          />
          <Button
            color="primary"
            onClick={createNewPesel}
            disabled={!valid || !touched || loading}
            style={styles.button}
          >
            {loading ? loader : 'Dodaj'}
          </Button>
          <div style={styles.feedback}>{feedBack}</div>
        </div>
        {props.count !== 0 ? deleteAll : null}
      </div>
    </div>
  )
}

NewPesel.defaultProps = {
  consultationId: '',
  onNewItem: (item) => console.warn('empty prop', item),
  isUnique: () => {},
  count: 0,
  deleteAll: () => {}
}

NewPesel.propTypes = {
  consultationId: PropTypes.string,
  onNewItem: PropTypes.func,
  isUnique: PropTypes.func,
  count: PropTypes.number,
  deleteAll: PropTypes.func
}

export default NewPesel
