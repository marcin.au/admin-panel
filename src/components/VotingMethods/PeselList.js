import React from 'react'
import PropTypes from 'prop-types'
import NewPesel from './NewPesel'
import Pesel from './Pesel'
import * as Alerts from './Alerts'
import * as Requests from './Requests'

const styles = {
  container: {
    maxHeight: 700
  }
}

const PeselList = (props) => {
  const [list, setList] = React.useState([])

  const scroll = list.length > 15

  const deleteItem = (selectedItem) => {
    setList(list => list.filter(item => item !== selectedItem))
  }

  const deleteAll = async (event) => {
    event.preventDefault()
    const confirmed = await Alerts.deletePrompt()
    if (!confirmed.value) return

    const result = await Requests.deleteAll({ consultationId: props.consultationId })
    if (!result) {
      Alerts.notify('Błąd podczas usuwania', null, 'error')
      return
    }
    Alerts.notify('Wszystkie numery pesel usunięte')
    setList([])
  }

  const pesels = list.map(item => (
    <Pesel
      data={item}
      consultationId={props.consultationId}
      key={item.id}
      deleteHandler={() => deleteItem(item)}
    />
  ))

  const isUnique = (pesel) => list.findIndex(item => item.value === pesel) === -1

  return (
    <div style={{ ...styles.container, overflowY: scroll ? 'scroll' : 'hidden' }}>
      <NewPesel
        consultationId={props.consultationId}
        onNewItem={(item) => setList(list => [item, ...list])}
        isUnique={isUnique}
        count={props.initialCount + list.length}
        deleteAll={deleteAll}
      />
      {pesels}
    </div>
  )
}

PeselList.defaultProps = {
  consultationId: '',
  initialCount: 0
}

PeselList.propTypes = {
  consultationId: PropTypes.string,
  initialCount: PropTypes.number
}

export default PeselList
