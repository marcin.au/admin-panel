import React from 'react'
import PropTypes from 'prop-types'
import { Input, Spinner } from 'reactstrap'
import HoverIcon from './HoverIcon'
import * as Requests from './Requests'
import * as Alerts from './Alerts'

const styles = {
  container: {
    marginTop: 6,
    display: 'flex',
    alignItems: 'center'
  },
  input: {
    width: 200,
  },
  icon: {
    marginLeft: 33
  },
  loader: {
    marginLeft: 42
  }
}

const hashPesel = (str) => {
  if (str.length !== 11) return str

  return `${str.substring(0,4)}*****${str.substring(9,11)}`
}

const Pesel = (props) => {
  const [highlight, setHighLight] = React.useState(true)
  const [loading, setLoading] = React.useState(false) // TODO

  React.useEffect(() => {
    setTimeout(() => setHighLight(false), 2000)
  }, [])
  
  const deleteItem = async () => {
    setLoading(true)
    const result = await Requests.deleteWhiteListItem({
      id: props.data.id,
      consultationId: props.consultationId
    })
    setLoading(false)
    if (!result) {
      Alerts.notify('Błąd przy usuwaniu numeru Pesel', null, 'error')
      return
    }
    props.deleteHandler()
  }
  
  const loader = <Spinner size="sm" style={styles.loader} />
  const icon = <HoverIcon onClick={deleteItem} style={styles.icon} />

  return (
    <div style={styles.container}>
      <Input
        value={hashPesel(props.data.value)}
        style={styles.input}
        disabled
        className={highlight ? 'is-valid' : null}
      />
      {loading ? loader : icon}
    </div>
  )
}

Pesel.defaultProps = {
  consultationId: '',
  data: { id: '', value: '' },
  deleteHandler: () => {}
}

Pesel.propTypes = {
  consultationId: PropTypes.string,
  data: PropTypes.shape({
    id: PropTypes.string,
    value: PropTypes.string
  }),
  deleteHandler: PropTypes.func
}

export default Pesel
