import Swal from 'sweetalert2'

export const deletePrompt = async (config) => {
  return await Swal.fire({
    title: config?.title ?? 'Czy jesteś pewien?',
    text: config?.text ?? "Tej operacji nie da się cofnąć",
    icon: config?.icon ?? 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Usuń',
    cancelButtonText: "Anuluj",
    reverseButtons: true
  })
}

export const notify = (text, description, type = 'success') => {
  Swal.fire({
    title: text,
    text: description,
    icon: type,
    timer: 1500,
    showConfirmButton: false,
    timerProgressBar: true
  })
}

// args = ( func, message )
export const withErrorHandler = async (args) => {
  try {
    await args.func()
  }
  catch {
    notify(args.message, null, 'error')
  }
}
