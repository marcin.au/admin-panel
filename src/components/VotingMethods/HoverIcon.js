import React from 'react'
import PropTypes from 'prop-types'

const HoverIcon = (props) => {
  const [hover, setHover] = React.useState(false)

  const styles = {
    icon: {
      fontSize: props.size
    },
    iconContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: props.size + 17,
      height: props.size + 17,
      cursor: 'pointer',
      borderRadius: (props.size + 17) / 2
    },
    hover: {
      background: '#ddd',
    }
  }

  return (
    <div
      style={{ ...styles.iconContainer, ...(hover ? styles.hover : {}), ...props.style }}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      onClick={props.onClick}
    >
      <i
        className={`${props.name} text-${props.color}`}
        style={styles.icon}
      />
    </div>
  )
}

HoverIcon.defaultProps = {
  onClick: () => {},
  color: 'red',
  size: 18,
  name: 'fas fa-trash',
  style: {}
}

HoverIcon.propTypes = {
  onClick: PropTypes.func,
  color: PropTypes.string,
  size: PropTypes.number,
  name: PropTypes.string,
  style: PropTypes.object
}

export default HoverIcon
