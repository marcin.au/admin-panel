import Api from '../../api'

// payload = ( data, consultationId )
export const createWhiteListItem = async (payload) => {
  const whitelist_item = { data: payload.data }
  const path = `/consultations/${payload.consultationId}/whitelist_items`
  const res = await Api({ withInstitution: true }).post(path, { whitelist_item })
  if (res.status === 201) return res.data.data.id

  if (res.status === 422) {
    const { errors } = res.data.data
    const taken = errors.find(item => (
      item.detail === 'has already been taken' &&
      item.source?.pointer === '/data/attributes/hashed_data'
    ))
    if (taken) return 'already-taken'
  }
}

// payload = ( id, consultationId )
export const deleteWhiteListItem = async (payload) => {
  const path = `/consultations/${payload.consultationId}/whitelist_items/${payload.id}`
  const res = await Api({ withInstitution: true }).delete(path)
  return res.status === 204
}

// payload = ( consultationId )
export const deleteAll = async (payload) => {
  const path = `/consultations/${payload.consultationId}/whitelist_items/clear`
  const res = await Api({ withInstitution: true }).delete(path)
  return res.status === 204
}
