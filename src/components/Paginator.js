/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect } from 'react'
import {
  CardFooter,
  Pagination,
  PaginationItem,
  PaginationLink,
  Button
} from "reactstrap"
import Api from '../api'
import Loader from '../components/Loader'
import { withRouter } from 'react-router'
import { useSelector } from 'react-redux'
import { currentInstitution } from '../utils'

const Paginator = props => {
  const [currentPage, setCurrentPage] = useState(1)
  const [loading, setLoading] = useState('hidden')
  const [meta, setMeta] = useState({})
  const institution = useSelector(state => state.auth.institution)

  const getPage = async page => {
    setLoading('progress')
    const apiInstance = props.skipInstitution ? Api() : Api({ withInstitution: true })
    const res = await apiInstance.get(`/${props.assetName}?page=${page}`)
    if (res.status === 200) {
      setLoading('success')
      props.setCollection(res.data.data)
      setMeta(res.data.meta)
    } else {
      setLoading('error')
    }
  }

  const previousHandler = () => {
    if (currentPage > 1) {
      getPage(currentPage - 1)
      setCurrentPage(currentPage => currentPage - 1)
    }
  }

  const nextHandler = () => {
    if (!meta) return
    if (meta.last > currentPage) {
      getPage(currentPage + 1)
      setCurrentPage(currentPage => currentPage + 1)
    }
  }

  useEffect(() => {
    getPage(currentPage)
  }, [institution])

  const button = (
    <Button
      color="primary"
      className="paginator-new-button"
      onClick={() => props.history.push(`/${currentInstitution()}/admin/${props.assetName}/new`)}
    >
      Nowy
    </Button>
  )

  return (
    <div className='paginator'>
      <Loader status={loading}/>
      <CardFooter className="py-4">
        <nav aria-label="...">
          {props.hideButton ? null : button}
          <Pagination
            className="pagination justify-content-end mb-0"
            listClassName="justify-content-end mb-0"
          >
            <PaginationItem className={currentPage <= 1 ? 'disabled' : null}>
              <PaginationLink
                onClick={previousHandler}
                tabIndex="-1"
              >
                <i className="fas fa-angle-left" />
                <span className="sr-only">Previous</span>
              </PaginationLink>
            </PaginationItem>
            <PaginationItem className="active">
              <PaginationLink onClick={e => e.preventDefault()}>
                {currentPage}
              </PaginationLink>
            </PaginationItem>
            <PaginationItem className={currentPage >= meta.last ? 'disabled' : null}>
              <PaginationLink onClick={nextHandler}>
                <i className="fas fa-angle-right" />
                <span className="sr-only">Next</span>
              </PaginationLink>
            </PaginationItem>
          </Pagination>
        </nav>
      </CardFooter>
    </div>
  )
}

export default withRouter(Paginator)
