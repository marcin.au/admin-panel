import React from 'react'
import '../styles/Loader.css'

const GlobalLoader = props => {
  return (
    <div className={`global-loader ${props.status}`}>
      <div className="lds-dual-ring"></div>
    </div>
  )
}

export default GlobalLoader