/* eslint-disable react-hooks/exhaustive-deps */

import React, {useEffect, useState} from 'react'
import * as SurveyJSCreator from "survey-creator"
import * as SurveyKo from "survey-knockout"
import "survey-creator/survey-creator.css"
import $ from "jquery"
import "pretty-checkbox/dist/pretty-checkbox.css"
import * as widgets from "surveyjs-widgets"
import PropTypes from 'prop-types'
import { Spinner } from 'reactstrap'

SurveyJSCreator.StylesManager.applyTheme("default");
widgets.prettycheckbox(SurveyKo);
widgets.select2(SurveyKo, $);
widgets.inputmask(SurveyKo);
widgets.jquerybarrating(SurveyKo, $);
widgets.jqueryuidatepicker(SurveyKo, $);
widgets.nouislider(SurveyKo);
widgets.select2tagbox(SurveyKo, $);
widgets.sortablejs(SurveyKo);
widgets.ckeditor(SurveyKo);
widgets.autocomplete(SurveyKo, $);
widgets.bootstrapslider(SurveyKo);

const styles = {
  loaderContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    height: 200
  },
  loader: {
    width: 60,
    height: 60
  },
  loaderText: {
    marginTop: 20
  }
}

const Survey = props => {
  const [dataLoaded, setDataLoaded] = useState(false)

  useEffect(() => {
    const init = async () => {
      if (!props.value || dataLoaded) return
      const options = { 
        showJSONEditorTab: false
      }

      // let defaultThemeColors = SurveyJSCreator.StylesManager.ThemeColors['default']
      // defaultThemeColors['$primary'] = 'blue'
      // defaultThemeColors['$primary-hover'] = 'blue'
      // defaultThemeColors['$secondary-color'] = 'blue'
      // defaultThemeColors['$selection-border-color'] = 'blue'
      // SurveyJSCreator.StylesManager.applyTheme('bootstrap')

      SurveyJSCreator.localization.currentLocale = "pl"
      let newInstance = new SurveyJSCreator.SurveyCreator('survey-creator-container', options)
      newInstance.JSON = props.value
      newInstance.saveSurveyFunc = () => props.onChange(JSON.parse(newInstance.text))
      newInstance.showToolbox = 'right'
      newInstance.showPropertyGrid = 'right'
      if (props.mode === 'show') {
        newInstance.showTestSurvey()
        newInstance.showDesignerTab = false
      }
      setDataLoaded(true)
    }
    init()
  }, [props.value])

  const loader = (
    <div style={styles.loaderContainer}>
      <div className="lds-dual-ring lds-dual-ring-black"></div>
      <span style={styles.loaderText}>Wczytywanie kreatora ankiet</span>
    </div>
  )

  return (
    <div style={{ padding: 10 }} id="survey-creator-container">
      {loader}
    </div>
  )
}

Survey.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.object,
  mode: PropTypes.oneOf(['show', 'edit'])
}

export default Survey
