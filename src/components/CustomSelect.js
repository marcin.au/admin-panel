import React from 'react'
import Select, { components } from 'react-select'
import PropTypes from 'prop-types'

const { Option, SingleValue } = components

const CustomOption = (props) => (
  <Option {...props}>
    {props.data.element}
  </Option>
)

const CustomSingleValue = (props) => (
  <SingleValue {...props}>
    {props.data.element}
  </SingleValue>
)

const CustomSelect = (props) => {
  return (
    <Select
      value={props.value}
      onChange={props.onChange}
      options={props.options}
      components={{ Option: CustomOption, SingleValue: CustomSingleValue }}
      styles={props.style}
      placeholder={props.placeholder}
    />
  )
}

CustomSelect.defaultProps = {
  value: null,
  options: [],
  onChange: () => {},
  style: {},
  placeholder: null
}

CustomSelect.propTypes = {
  value: PropTypes.shape({
    value: PropTypes.string,
    element: PropTypes.node
  }),
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string,
    element: PropTypes.node
  })),
  onChange: PropTypes.func,
  style: PropTypes.object,
  placeholder: PropTypes.string
}

export default CustomSelect
