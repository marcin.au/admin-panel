import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'reactstrap';

const ContentTable = props => {

  const headers = props.headers.map(item => <th key={item} scope="col">{item}</th>)

  return (
    <div className='index-table'>
      <Table className="align-items-center table-flush" responsive>
        <thead className="thead-light">
          <tr>{headers}</tr>
        </thead>
        <tbody className='bg-white'>{props.children}</tbody>
      </Table>
    </div>
  )
}

ContentTable.propTypes = {
  headers: PropTypes.array.isRequired
}

export default ContentTable;