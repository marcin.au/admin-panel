import React, {useCallback, useState, useEffect} from 'react'
import { useDropzone } from 'react-dropzone'
import { PaginationProvider } from 'react-bootstrap-table2-paginator'

const Dropdown = props => {

  const [showImage, setShowImage] = useState()
  const [isImage, setIsImage] = useState(false)
  const [imgAv, setImgAv] = useState(false)
  const setImage = props.setImage
  const imgAvailable = props.imgAvailable
  const imageUrl = props.imageUrl
  const deleteImg = props.deleteImg
  const isDeleted = props.isDeleted

  const onDrop = useCallback(acceptedFiles => {
    let reader = new FileReader()
    reader.readAsDataURL(acceptedFiles[0])
    reader.onload = () => {setShowImage(reader.result)}
    setImage(acceptedFiles[0])
    setIsImage(true)
  }, [])

  const deleteImage = () => {
    setIsImage(false)
    setShowImage()
    setImage()
  }

  const deleteImageFromServer = () => {
    deleteImg()
  }

  useEffect(() => {
    if(!isDeleted){
      setImgAv(isDeleted)
      setIsImage(isDeleted)
    }
  }, [isDeleted])
  
  useEffect(()=> {
    if(imgAvailable){
      setIsImage(true)
      setImgAv(true)
    }
  },[imgAvailable])



  const {getRootProps, getInputProps, acceptedFiles} = useDropzone({onDrop})

  const files = acceptedFiles.map(file => (
    <div key={file.path} style={styles.fileBox}>
       <div style={styles.infoBox}>
        <div style={styles.imageBox}>
          <img src={showImage} style={styles.image} alt=""/>
        </div> 
          <div style={styles.fileInfo}>
            <div style={styles.fileName}>{file.path}</div>
            <div style={styles.fileSize}>{parseFloat(file.size / 1024).toFixed(2)}KB</div>
          </div>
       </div>
       <i className={'fas fa-trash text-white'} style={styles.deleteImage} onClick={() => {deleteImage()}} ></i>
    </div>
  ));

  const imageFromServer = (
    <div style={styles.fileBox}>
    <div style={styles.infoBox}>
     <div style={styles.imageBox}>
       <img src={imageUrl} style={styles.image} alt=""/>
     </div> 
       <div style={styles.fileInfo}>
         <div style={styles.fileName}>Name</div>
       </div>
    </div>
    <i className={'fas fa-trash text-white'} style={styles.deleteImage} onClick={() => {deleteImageFromServer()}} ></i>
 </div>
  )

  const dropzone = (
    <section className="container">
    <div {...getRootProps()} style={styles.dropzone}>
      <input {...getInputProps()} />
      <p>Upuść zdjęcie tutaj lub kliknij aby wybrać zdjęcie</p>
    </div>
  </section>
  )

  if(imgAv) return imageFromServer
  if(isImage) return files
  if(!isImage) return dropzone
  
}

const styles = {
  dropzone: {
    display: 'flex',
    justifyContent: "center",
    alignItems: "center",
    border: 'dashed 2px #ccc',
    width: '100%',
    backgroundColor: '#efefef',
    height: 150,
    borderRadius: 4,
  },
  fileBox: {
    display: 'flex',
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
    height: 150
  },
  infoBox: {
    display: 'flex',
  },
  imageBox: { 
    display: "flex",
    alignItems: 'center',
    justifyContent: "center",
    marginRight: 15, 
    height: 110, 
    width: 110,
    overflow: "hidden",
    backgroundColor: '#ccc',
    borderRadius: 10
  },
  deleteImage: {
    fontSize: 15, 
    fontWeight: 'bold',
    width: 30, 
    height: 30, 
    borderRadius:50 , 
    background: 'red', 
    display: 'flex', 
    justifyContent:"center", 
    alignItems: "center", 
    color: "white", 
    opacity: 0.9, 
    cursor: 'pointer'
  },
  fileInfo: {
    width: 350,
    textAlign: 'left',
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  fileName: {
    fontSize: 15,
    fontWeight: "bold",
    color: "grey"
  },
  fileSize: {
    fontSize: 13,
    color: '#aaa'
  },
  image: {
   width: 100,
  },
  imageFromServer: {
    maxHeight: 500
  }
}

export default Dropdown