import React from 'react'
import Api from '../api'
import CustomSelect from './CustomSelect'
import { useDispatch, useSelector } from 'react-redux'
import { changeInstitution as changeInstitutionAction } from '../redux/actionCreators/auth'

const API_URL = process.env.REACT_APP_IMAGE_STORAGE_URL ?? ''

const fetchInstitutions = async () => {
  const res = await Api().get('/institutions')
  if (res.status !== 200) return []

  return res.data.data.map(institution => ({
    id: institution.id,
    ...institution.attributes
  }))
}

const styles = {
  container: {
    width: 300,
    marginRight: 20,
  },
  option: {
    display: 'flex',
  },
  icon: {
    fontSize: 20
  },
  image: {
    width: 34,
    height: 34,
    borderRadius: 17,
  },
  name: {
    fontSize: 17,
    color: '#222',
    display: 'flex',
    alignItems: 'center',
    height: 40,
    marginLeft: 15
  },
  logo: {
    border: 'solid 3px #555',
    borderRadius: 20,
    width: 40,
    height: 40,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
}

const InstitutionSelect = () => {
  const [institutions, setInstitutions] = React.useState([])
  const dispatch = useDispatch()
  const selectedInstitution = useSelector(state => state.auth.institution)

  React.useEffect(() => {
    (async () => {
      const institutions = await fetchInstitutions()
      setInstitutions(institutions)
      const institution = institutions.find(item => item.slug === selectedInstitution.slug)
      if (!institution) return

      await dispatch(changeInstitutionAction(institution))
    })()
  }, [])

  const changeInstitution = async (id) => {
    const institution = institutions.find(item => item.id === id)
    if (!institution) return

    await dispatch(changeInstitutionAction(institution))
  }

  const getSelectedInstitution = () => {
    if (!selectedInstitution) return null

    return { value: selectedInstitution.id, element: institutionElement(selectedInstitution) }
  }

  const institutionElement = (institution) => {
    const logo = institution.logo_url ? `${API_URL}${institution.logo_url}` : null
    const image = <img src={logo} style={styles.image} alt='logo' />
    const placeholder = <i style={styles.icon} className="ni ni-building" />
    return (
      <div style={styles.option}>
        <div style={styles.logo}>
          {logo ? image : placeholder}
        </div>
        <div style={styles.name}>{institution.name}</div>
      </div>
    )
  }

  const options = institutions.map(institution => (
    { value: institution.id, element: institutionElement(institution) }
  ))

  return (
    <div style={styles.container}>
      <CustomSelect
        value={getSelectedInstitution()}
        options={options}
        placeholder="Instytucja..."
        onChange={element => changeInstitution(element.value)}
        style={{ input: styles => ({ ...styles, height: 50 }) }}
      />
    </div>
  )
}

export default InstitutionSelect
