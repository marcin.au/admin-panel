import { resource } from './utils'

import Login from "views/Login.js"
import Logout from 'views/Logout'

import Profile from 'views/UserProfile/Profile'
import EditProfile from 'views/UserProfile/EditProfile'

import Icons from 'views/UserProfile/Icons'

import CurrentAffairs from 'views/CurrentAffairs/CurrentAffairs'
import NewCurrentAffairs from 'views/CurrentAffairs/NewCurrentAffairs'
import EditCurrentAffairs from 'views/CurrentAffairs/EditCurrentAffairs'
import ShowCurrentAffairs  from 'views/CurrentAffairs/ShowCurrentAffairs'

import Users from 'views/Users/Users'
import NewUser from 'views/Users/NewUser'
import ShowUser from 'views/Users/ShowUser'
import EditUser from 'views/Users/EditUser'

import NewInstitution from 'views/Institutions/NewInstitution'
import EditInstitution from 'views/Institutions/EditInstitution'
import ShowInstitution from 'views/Institutions/ShowInstitution'
import Institutions from 'views/Institutions/Institutions'

import NewConsultation from 'views/Consultations/NewConsultation'
import EditConsultations from 'views/Consultations/EditConsultation'
import ShowConsultation from 'views/Consultations/ShowConsultation'
import Consultations from 'views/Consultations/Consultations'

import Propositions from 'views/Propositions/Propositions'
import ShowProposition from 'views/Propositions/ShowProposition'

import Sections from 'views/Sections/Container'

var routes = [
  {
    path: "/login",
    name: "Login",
    icon: "ni ni-key-25 text-info",
    component: Login,
    layout: "/auth",
    display: false
  },
  ...resource({
    path: 'current_affairs',
    name: 'Aktualności',
    icon: {
      name: 'notification-70',
      color: 'red'
    },
    components: {
      index: CurrentAffairs,
      show: ShowCurrentAffairs,
      new: NewCurrentAffairs,
      edit: EditCurrentAffairs
    }
  }),
  ...resource({
    path: 'users',
    name: 'Użytkownicy',
    icon: {
      name: 'single-02',
      color: 'blue'
    },
    components: {
      index: Users,
      show: ShowUser,
      new: NewUser,
      edit: EditUser
    }
  }),
  ...resource({
    path: 'institutions',
    name: 'Gminy',
    icon: {
      name: 'building',
      color: 'yellow'
    },
    components: {
      index: Institutions,
      show: ShowInstitution,
      new: NewInstitution,
      edit: EditInstitution
    },
    showWithoutInstitution: true,
  }),
  ...resource({
    path: 'consultations',
    name: 'Konsultacje',
    icon: {
      name: 'archive-2',
      color: 'orange'
    },
    components: {
      index: Consultations,
      show: ShowConsultation,
      new: NewConsultation,
      edit: EditConsultations
    }
  }),
  {
    path: '/icons',
    component: Icons,
    display: false,
    layout: "/admin"
  },
  {
    path: '/user_profile/edit',
    component: EditProfile,
    display: false,
    layout: "/admin"
  },
  {
    path: '/user_profile',
    component: Profile,
    display: false,
    layout: "/admin"
  },
  {
    path: "/propositions/:id",
    component: ShowProposition,
    display: false,
    layout: "/admin"
  },
  {
    path: "/propositions",
    name: "Propozycje",
    icon: "ni ni-send text-pink",
    component: Propositions,
    layout: "/admin"
  },
  {
    path: "/sections",
    name: "Podstrony i sekcje",
    icon: "ni ni-ungroup text-blue",
    component: Sections,
    layout: "/admin"
  },
  {
    path: '/logout',
    name: 'Wyloguj',
    icon: "ni ni-user-run text-red",
    component: Logout,
    layout: '/admin',
    showWithoutInstitution: true
  }
]

export default routes
