import moment from 'moment'
import store from './redux/store'

export const parseDate = date => moment(date).format('LLL')

export const getLocaDate = (date) => moment(date).format('YYYY-MM-DD')

export const idFromPath = () => window.location.pathname.match(/\d+/)

export const initialFormState = (fields) => {
  let obj = {}
  fields.forEach(item => {
    obj[item.name] = { value: '', touched: item.touched ?? false, errors: false, required: item.required }
  });
  return obj
}

export const formPayload = (form) => {
  let payload = {}
  Object.keys(form).forEach(name => {
    if (!form[name].value || form[name].value === '') {
      payload[name] = undefined
      return
    }
    payload[name] = form[name].value
  })
  console.log(payload)
  return payload
}

export const parseErrors = (errors) => {
  if (!errors) return ''

  const content = errors.map(err => `<li><b>${err.source.pointer}: </b>${err.detail}</li>`).join('')
  return `<ul>${content}</ul>`
}

export const currentInstitution = () => {
  const institution = store.getState().auth.institution?.slug
  return !institution || institution === '' ? 'super' : institution
}

// config = ( path, name, icon ( name, color ), components ( show, edit, index, new )
// showWithoutInstitution )
export const resource = (config) => {
  return [
    {
      path: `/${config.path}/new`,
      component: config.components.new,
      layout: "/admin",
      display: false
    },
    {
      path: `/${config.path}/:id/edit`,
      component: config.components.edit,
      display: false,
      layout: "/admin"
    },
    {
      path: `/${config.path}/:id`,
      component: config.components.show,
      display: false,
      layout: "/admin"
    },
    {
      path: `/${config.path}`,
      name: config.name,
      icon: `ni ni-${config.icon.name} text-${config.icon.color}`,
      component: config.components.index,
      layout: "/admin",
      showWithoutInstitution: config.showWithoutInstitution
    },
  ]
}
