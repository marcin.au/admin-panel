import * as actions from '../actions/sections'

const initialState = {
  data: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.FETCH_SECTIONS:
      return fetchSections(state, action.payload)
    case actions.CREATE_SECTION:
      return createSection(state, action.payload)
    case actions.UPDATE_SECTION:
      return updateSection(state, action.payload)
    case actions.DELETE_SECTION:
      return deleteSection(state, action.payload)
    case actions.CREATE_PAGE:
      return createPage(state, action.payload)
    case actions.UPDATE_PAGE:
      return updatePage(state, action.payload)
    case actions.DELETE_PAGE:
      return deletePage(state, action.payload)
    default:
      return state
  }
}

// payload = res.data
const fetchSections = (state, payload) => {
  const sectionsForPage = (pageId) => {
    const included = payload.included.filter(item => (
      item.type === 'section' && item?.relationships?.page?.data?.id === pageId
    ))
    return  included.map(item => ({
      id: item.id,
      name: item.attributes.title,
      content: item.attributes.body,
      page_id: pageId
    }))
  }
  const newState = payload.data.map(page => ({
    id: page.id,
    name: page.attributes.title,
    content: page.attributes.body,
    sections: sectionsForPage(page.id)
  }))
  return { ...state, data: newState }
}

// payload = ( id, name, content, page_id )
const createSection = (state, payload) => {
  const newState = state.data.map(page => {
    if (page.id !== payload.page_id) return page

    const newSections = [...page.sections, payload]
    return { ...page, sections: newSections }
  })
  return { ...state, data: newState }
}

// payload = ( id, name, content, page_id )
const updateSection = (state, payload) => {
  const newState = state.data.map(page => {
    if (page.id !== payload.page_id) return page

    const newSections = page.sections.map(section => {
      if (section.id !== payload.id) return section

      return { ...section, ...payload }
    })

    return { ...page, sections: newSections }
  })
  return { ...state, data: newState }
}

// payload = ( id, page_id )
const deleteSection = (state, payload) => {
  const newState = state.data.map(page => {
    if (page.id !== payload.page_id) return page

    const newSections = page.sections.filter(section => section.id !== payload.id)

    return { ...page, sections: newSections }
  })
  return { state, data: newState }
}

// payload = ( name, content, id )
const createPage = (state, payload) => {
  const newState = [...state.data, { ...payload, sections: [] }]
  return { ...state, data: newState }
}

// payload = ( id, name, content )
const updatePage = (state, payload) => {
  const newState = state.data.map(page => {
    if (page.id !== payload.id) return page

    const { name, content } = payload
    return { ...page, name, content }
  })

  return { ...state, data: newState }
}

// payload = ( id )
const deletePage = (state, payload) => {
  const newState = state.data.filter(page => page.id !== payload.id)
  return { ...state, data: newState }
}
