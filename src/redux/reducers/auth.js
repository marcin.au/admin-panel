import * as actions from '../actions/auth'

const initialInstitution = () => {
  const arr = document.location.pathname.split('/')
  return !arr[1] || arr[1] === '' ? 'super' : arr[1]
}

const initialState = {
  loggedIn: false,
  currentUser: {},
  id: null,
  token: null,
  role: null,
  userData: {},
  userId: null,
  institution: { slug: initialInstitution() }
}

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.LOGIN:
      const { currentUser, token, role, id } = action
      return { ...state, userData: currentUser, currentUser, token, role, loggedIn: true, id }
    case actions.LOGOUT:
      return { ...state, userData: {}, currentUser: {}, token: null, role: null, loggedIn: false }
    case actions.SET_ID:
      return { ...state, id: action.id }
    case actions.SET_TOKEN:
      return { ...state, token: action.token }
    case actions.EDIT_USER:
       return {...state, userData: action.userData, userId: action.userId}
    case actions.SET_INSTITUTION:
      return { ...state, institution: action.institution }
    default:
      return state
  }
}
