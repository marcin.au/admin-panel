import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import ReduxThunk from 'redux-thunk'
import authReducer from './reducers/auth'
import sectionsReducer from './reducers/sections'

const reducer = combineReducers({
  auth: authReducer,
  sections: sectionsReducer
})

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer,
  composeEnhancer(applyMiddleware(ReduxThunk)),
)

export default store
