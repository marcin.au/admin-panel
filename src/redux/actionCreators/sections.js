import * as actions from '../actions/sections'
import Api from '../../api'

export const fetchSections = () => {
  return async dispatch => {
    const res = await Api({ withInstitution: true }).get('/pages')
    if (res.status !== 200) throw new Error('err')

    dispatch({ type: actions.FETCH_SECTIONS, payload: res.data })
  }
}

// payload = ( name, content, page_id )
export const createSection = (payload) => {
  return async dispatch => {
    const section = {
      title: payload.name,
      body: payload.content
    }
    const path = `/pages/${payload.page_id}/sections`
    const res = await Api({ withInstitution: true }).post(path, { section })
    if (res.status !== 201) throw new Error('err')

    const { id } = res.data.data
    dispatch({ type: actions.CREATE_SECTION, payload: { ...payload, id } })
  }
}

// payload = ( id, name, content, page_id )
export const updateSection = (payload) => {
  return async dispatch => {
    const section = {
      title: payload.name,
      body: payload.content
    }
    const path = `/pages/${payload.page_id}/sections/${payload.id}`
    const res = await Api({ withInstitution: true }).put(path, { section })
    if (res.status !== 200) throw new Error('err')

    dispatch({ type: actions.UPDATE_SECTION, payload })
  }
}

// payload = ( id, page_id )
export const deleteSection = (payload) => {
  return async dispatch => {
    const path = `/pages/${payload.page_id}/sections/${payload.id}`
    const res = await Api({ withInstitution: true }).delete(path)
    if (res.status !== 204) throw new Error('err')

    dispatch({ type: actions.DELETE_SECTION, payload })
  }
}

// payload = ( name, content )
export const createPage = (payload) => {
  return async dispatch => {
    const page = {
      title: payload.name,
      body: payload.content
    }
    const res = await Api({ withInstitution: true }).post('/pages', { page })
    if (res.status !== 201) throw new Error('err')

    const { id } = res.data.data
    dispatch({ type: actions.CREATE_PAGE, payload: { ...payload, id } })
  }
}

// payload = ( id, name, content )
export const updatePage = (payload) => {
  return async dispatch => {
    const page = {
      title: payload.name,
      body: payload.content
    }
    const res = await Api({ withInstitution: true }).put(`/pages/${payload.id}`, { page })
    if (res.status !== 200) throw new Error('err')

    dispatch({ type: actions.UPDATE_PAGE, payload })
  }
}

// payload = ( id )
export const deletePage = (payload) => {
  return async dispatch => {
    const res = await Api({ withInstitution: true }).delete(`/pages/${payload.id}`)
    if (res.status !== 204) throw new Error('err')

    dispatch({ type: actions.DELETE_PAGE, payload })
  }
}
