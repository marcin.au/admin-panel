import * as actions from '../actions/auth'
import Axios from 'axios'
import Api from '../../api'
import { url as baseUrl } from '../../api'

export const login = (email, password) => {
  return async dispatch => {
    const payload = { user: { email, password } }
    try {
      const res = await Axios.post(`${baseUrl}/sessions`, payload)
      if (res.status === 201) {
        const currentUser = { id: res.data.data.id, ...res.data.data.attributes }
        const role = currentUser.role
        const token = res.headers.authorization
        dispatch({ type: actions.LOGIN, currentUser, role, token, id: currentUser.id })
        localStorage.setItem('token', token)
        localStorage.setItem('id', currentUser.id)
        return
      }
      throw new Error('unauthorized')
    }
    catch (err) {
      throw new Error('unauthorized')
    }
  }
}

export const logout = () => {
  return dispatch => {
    dispatch({ type: actions.LOGOUT })
    localStorage.removeItem('token')
    localStorage.removeItem('id')
  }
}

export const fillUserInfo = (id, token) => {
  return async dispatch => {
    try {
      const res = await Axios.get(`${baseUrl}/users/me`, {
        headers: { Authorization: token }
      })
      if (res.status !== 200) throw new Error('error')
      console.log(res)

      const currentUser = { id: res.data.data.id, ...res.data.data.attributes }
      const role = currentUser.role
      
      dispatch({ type: actions.LOGIN, currentUser, role, token, id })
    }
    catch (err) {
      throw new Error('error')
    }
    
  }
}

export const updateProfile = (userId, userData) => {
  return async (dispatch, getState) => {
    const token = getState().auth.token
    const res = await Axios.put(`${baseUrl}/users/${userId}`, {user: userData}, {headers: { 'Authorization': token }})
    if (res.status !== 200) throw new Error('error')
    dispatch({type: actions.EDIT_USER, userData, userId})
  }
}

export const changeInstitution = (institution) => {
  return async dispatch => {
    let path = window.location.pathname.split('/')
    if (path.length < 2) return

    path[1] = institution.slug
    window.history.pushState(null, null, path.join('/'))
    dispatch({ type: actions.SET_INSTITUTION, institution })
  }
}
