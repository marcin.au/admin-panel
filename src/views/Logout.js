import { useEffect } from 'react'
import Swal from 'sweetalert2'
import { useDispatch } from 'react-redux'
import { logout } from 'redux/actionCreators/auth'
import { currentInstitution } from '../utils'

const Logout = props => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(logout())
    props.history.push(`/${currentInstitution()}/login`)
    Swal.fire({
      title: 'Wylogowano',
      icon: 'success',
      timer: 1000,
      timerProgressBar: true,
      showConfirmButton: false
    })
  }, [props.history, dispatch])

  return null
}

export default Logout
