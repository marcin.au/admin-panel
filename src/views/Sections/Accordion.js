import React from 'react'
import { Collapse, Card } from 'reactstrap'
import PropTypes from 'prop-types'
import colors from '../../variables/colors'

const styles = {
  container: {
    width: '100%',
    padding: 10
  },
  arrow: {
    fontSize: 25,
    color: colors.primary,
    marginRight: 20
  },
  content: {
    marginTop: 15
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    fontSize: 16,
    cursor: 'pointer'
  }
}

const Accordion = (props) => {
  const [open, setOpen] = React.useState(false)
  const iconName = `ni ni-bold-${open ? 'up' : 'down'}`

  const toggle = () => setOpen(open => !open)

  return (
    <Card style={styles.container}>
      <div style={styles.header} onClick={toggle}>
        <i className={iconName} style={styles.arrow} />
        {props.title}
      </div>
      <Collapse isOpen={open}>
        <div style={styles.content}>
          {props.children}
        </div>
      </Collapse>
    </Card>
  )
}

Accordion.defaultProps = {
  title: 'Empty title',
  children: []
}

Accordion.propTypes = {
  title: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
}

export default Accordion
