import React from 'react'
import PropTypes from 'prop-types'
import colors from '../../variables/colors'
import { Button } from 'reactstrap'
import ActionIcon from './ActionIcon'
import Editor from './Editor'
import { useDispatch } from 'react-redux'
import * as ActionCreators from '../../redux/actionCreators/sections'
import { withErrorHandler, deletePrompt } from './Alerts'

const styles = {
  container: {
    width: 300,
    height: 80,
    display: 'inline-block',
    margin: 10
  },
  content: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
    flexDirection: 'column'
  },
  plus: {
    fontSize: 40,
    color: colors.primary
  }
}

const Section = (props) => {
  const [hover, setHover] = React.useState(false)
  const [showEditor, setShowEditor] = React.useState(false)
  const dispatch = useDispatch()

  const createNewSection = async (name, content) => {
    const payload = {
      name,
      content,
      page_id: props.page_id
    }
    withErrorHandler({
      func: async () => await dispatch(ActionCreators.createSection(payload)),
      message: 'Błąd podczas dodawania sekcji'
    })
    setShowEditor(false)
  }
  
  const updateSection = async (name, content) => {
    const payload = {
      name,
      content,
      id: props.id,
      page_id: props.page_id
    }
    withErrorHandler({
      func: async () => await dispatch(ActionCreators.updateSection(payload)),
      message: 'Błąd podczas aktualizacji sekcji'
    })
    setShowEditor(false)
  }

  const deleteSection = async (event) => {
    event.stopPropagation()
    const confirmed = await deletePrompt({ text: '' })
    if (!confirmed.value) return

    const payload = {
      id: props.id, 
      page_id: props.page_id
    }
    withErrorHandler({
      func: async () => await dispatch(ActionCreators.deleteSection(payload)),
      message: 'Błąd podczas usuwania sekcji'
    })
  }

  const hoverContent = (
    <div>
      <ActionIcon name="fat-remove" color="red" size={40} onClick={deleteSection} />
    </div>
  )

  const shortName = () => {
    if (props.name.length < 30) return props.name
    return `${props.name.substring(0, 30)}...`
  }

  const content = (
    <div style={styles.content}>
      {shortName()}
      {hover ? hoverContent : null}
    </div>
  )

  const newContent = (
    <div style={styles.content}>
      <i className="ni ni-fat-add" style={styles.plus}/>
    </div>
  )


  const editor = (
    <Editor
      saveHandler={props.type === 'new' ? createNewSection : updateSection}
      cancelHandler={() => setShowEditor(false)}
      initialValue={{ name: props.name, content: props.content }}
      type="section"
    />
  )

  return (
    <React.Fragment>
      <Button
        style={styles.container}
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
        onClick={() => setShowEditor(true)}
      >
        {props.type === 'edit' ? content : newContent}
      </Button>
      {showEditor ? editor : null}
    </React.Fragment>

  )
}

Section.defaultProps = {
  id: null,
  name: '',
  content: '',
  page_id: '',
  type: 'edit'
}

Section.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  content: PropTypes.string,
  page_id: PropTypes.string,
  type: PropTypes.oneOf(['edit', 'new'])
}

export default Section
