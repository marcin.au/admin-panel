import React from 'react'
import PropTypes from 'prop-types'

const ActionIcon = (props) => {

  const styles = {
    container: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    icon: {
      fontSize: props.size
    }
  }

  return (
    <div style={styles.container} onClick={props.onClick}>
      <i
        className={`ni ni-${props.name} text-${props.color}`}
        style={styles.icon}
      />
    </div>
  )
}

ActionIcon.defaultProps = {
  title: 'no-icon',
  color: 'blue',
  size: 20,
  onClick: () => {}
}

ActionIcon.propTypes = {
  name: PropTypes.string,
  color: PropTypes.string,
  size: PropTypes.number,
  onClick: PropTypes.func
}

export default ActionIcon
