import React from 'react'
import PropTypes from 'prop-types'
import { Button, Nav, NavItem, NavLink, TabContent, TabPane, Card } from 'reactstrap'
import Accordion from './Accordion'
import Section from './Section'
import ReactHtmlParser from 'react-html-parser'
import { useDispatch } from 'react-redux'
import * as ActionCreators from '../../redux/actionCreators/sections'
import Editor from './Editor'
import classNames from 'classnames'
import * as Alerts from './Alerts'
import { withErrorHandler } from './Alerts'

const styles = {
  container: {
    marginTop: 40,
    marginBottom: 20
  },
  content: {
    margin: 10,
    overflow: 'hidden'
  },
  sections: {
    marginTop: 20
  },
  buttons: {
    margin: 10
  },
  tabContent: {
    paddingTop: 20
  },
  tab: {
    fontSize: 20,
    cursor: 'pointer'
  }
}

const Page = (props) => {
  const dispatch = useDispatch()
  const [showEditor, setShowEditor] = React.useState(false)
  const [tab, setTab] = React.useState(1)

  const sections = props.sections.map(section => (
    <Section
      key={section.id}
      id={section.id}
      name={section.name}
      content={section.content}
      page_id={props.id}
    />
  ))

  const deletePage = async () => {
    const payload = { id: props.id }
    const confirmed = (await Alerts.deletePrompt())?.value
    if (!confirmed) return

    withErrorHandler({
      func: async () => await dispatch(ActionCreators.deletePage(payload)),
      message: 'Błąd podczas usuwania strony'
    })
  }

  const updatePage = async (name, content) => {
    const payload = { id: props.id, name, content }
    withErrorHandler({
      func: async () => await dispatch(ActionCreators.updatePage(payload)),
      message: 'Błąd podczas aktualizacji strony'
    })
    setShowEditor(false)
  }

  const editor = (
    <Editor
      saveHandler={updatePage}
      cancelHandler={() => setShowEditor(false)}
      initialValue={{ name: props.name, content: props.content }}
      type="page"
    />
  )

  return (
    <Accordion title={props.name}>
      <div style={styles.container}>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classNames({ active: tab === 1 })}
              onClick={() => setTab(1) }
              style={styles.tab}
            >
              Podstrona
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classNames({ active: tab === 2 })}
              onClick={() => setTab(2) }
              style={styles.tab}
            >
              Sekcje
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={tab}>
          <TabPane tabId={1} style={styles.tabContent}>
            <div style={styles.content}>
              {ReactHtmlParser(props.content)}
            </div>
            <hr className="my-4" />
            <div style={styles.buttons}>
              <Button color="primary" onClick={() => setShowEditor(true)}>Edytuj</Button>
              <Button color="danger" onClick={deletePage}>Usuń</Button>
            </div>
          </TabPane>
          <TabPane tabId={2} style={styles.tabContent}>
            <div style={styles.sections}>
              {sections}
              <Section type="new" page_id={props.id}/>
            </div>
          </TabPane>
        </TabContent>
      </div>
      {showEditor ? editor : null}
    </Accordion>
  )
}

Page.defaultProps = {
  id: null,
  name: 'Empty name',
  content: 'Empty content',
  sections: []
}

Page.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  content: PropTypes.string,
  sections: PropTypes.array
}

export default Page
