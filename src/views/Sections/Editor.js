import React from 'react'
import PropTypes from 'prop-types'
import { Editor as TinyEditor } from '@tinymce/tinymce-react'
import { Card, Input, Button, Spinner } from 'reactstrap'
import colors from '../../variables/colors'
import Modal from './Modal'

const styles = {
  container: {
    zIndex: 100,
    width: '70%',
    height: 570,
    position: 'fixed',
    top: window.innerHeight / 4,
    left: window.innerWidth / 6,
    padding: 20,
    backgroundColor: colors.grey
  },
  name: {
    marginBottom: 20
  },
  buttons: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  button: {
    marginTop: 20,
    width: 200
  },
  loader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: 400
  }
}

const editorSettings = {
  height: 400,
  menubar: true,
  language: 'pl',
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table paste code help wordcount'
  ],
  toolbar: [
    'undo redo',
    'formatselect',
    'bold italic backcolor',
    'alignleft aligncenter alignright alignjustify',
    'bullist numlist outdent indent',
    'removeformat',
    'help'
  ].join(' | ')
}

const Editor = (props) => {
  const [name, setName] = React.useState(props.initialValue.name)
  const [content, setContent] = React.useState(props.initialValue.content)
  const [loaded, setLoaded] = React.useState(false)

  const onLoad = (editor) => {
    editor.on('init', () => setLoaded(true))
  }

  const loader = (
    <span style={styles.loader}>
      <Spinner color="primary" size="lg"/>
    </span>
  )

  return (
    <Modal visible={true} onClick={props.cancelHandler}>
      <Card style={styles.container}>
        <Input
          value={name}
          onChange={(e) => setName(e.target.value)}
          style={styles.name}
          placeholder={`Nazwa ${props.type === 'section' ? 'Sekcji' : 'Strony'}`}
        />
        <span style={{ display: loaded ? null : 'none' }}>
          <TinyEditor
            initialValue={content}
            value={content}
            init={{ ...editorSettings, setup: onLoad }}
            onEditorChange={(e) => setContent(e)}
          />
        </span>
        {loaded ? null : loader}
        <div style={styles.buttons}>
          <Button
            onClick={() => props.saveHandler(name, content)}
            color="primary"
            style={styles.button}
            disabled={name === ''}
          >
            Zapisz
          </Button>
          <Button
            onClick={props.cancelHandler}
            color="danger"
            style={styles.button}
          >
            Anuluj
          </Button>
        </div>
      </Card>
    </Modal>

  )
}

Editor.defaultProps = {
  saveHandler: (name, content) => {
    console.log(name, content)
  },
  cancelHandler: () => {},
  type: 'section',
  initialValue: {
    name: '',
    content: ''
  }
}

Editor.propTypes = {
  saveHandler: PropTypes.func,
  cancelHandler: PropTypes.func,
  type: PropTypes.oneOf(['section', 'page']),
  initialValue: PropTypes.shape({
    name: PropTypes.string,
    content: PropTypes.string
  })
}

export default Editor
