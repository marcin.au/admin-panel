import React from 'react'
import PropTypes from 'prop-types'

const styles = {
  container: {
    backgroundColor: '#000b',
    width: '100vw',
    height: '100vh',
    position: 'fixed',
    top: 0,
    left: 0,
    zIndex: 10
  }
}

const Modal = (props) => {
  return (
    <React.Fragment>
      <div style={styles.container} onClick={props.onClick} />
      {props.children}
    </React.Fragment>
  )
}

Modal.defaultProps = {
  visible: false,
  onClick: () => {},
  children: []
}

Modal.propTypes = {
  title: PropTypes.bool,
  onClick: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
}

export default Modal
