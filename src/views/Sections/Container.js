import React from 'react'
import ContentWrapper from 'components/ContentWrapper'
import Page from './Page'
import { Button } from 'reactstrap'
import { useSelector, useDispatch } from 'react-redux'
import Editor from './Editor'
import * as ActionCreators from '../../redux/actionCreators/sections'
import { withErrorHandler } from './Alerts'
import './style.css'

const styles = {
  container: {
    padding: 20,
    width: '100%',
    height: '100%'
  },
  button: {
    marginTop: 20
  }
}

const Container = () => {
  const data = useSelector(state => state.sections.data)
  const [showEditor, setShowEditor] = React.useState(false)
  const dispatch = useDispatch()

  React.useEffect(() => {
    withErrorHandler({
      func: async () => await dispatch(ActionCreators.fetchSections()),
      message: 'Błąd podczas pobierania zawartości'
    })
  }, [])

  const createPage = async (name, content) => {
    const payload = { name, content }
    withErrorHandler({
      func: async () => await dispatch(ActionCreators.createPage(payload)),
      message: 'Błąd podczas tworzenia strony'
    })
    setShowEditor(false)
  }

  const pages = data.map(page => (
    <Page
      key={page.id}
      id={page.id}
      name={page.name}
      content={page.content}
      sections={page.sections}
    />
  ))

  const editor = (
    <Editor
      saveHandler={createPage}
      cancelHandler={() => setShowEditor(false)}
      type="page"
    />
  )

  return (
    <ContentWrapper title="Podstrony i Sekcje">
      <div style={styles.container}>
        {pages}
        <Button color="primary" style={styles.button} onClick={() => setShowEditor(true)}>Dodaj nową stronę</Button>
      </div>
      {showEditor ? editor : null}
    </ContentWrapper>
  )
}

export default Container
