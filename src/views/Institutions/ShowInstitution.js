/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect } from 'react'
import ContentWrapper from '../../components/ContentWrapper'
import { Card, Row, Col, Button, CardBody, Form, FormGroup, Label } from 'reactstrap'
import api from 'api'
import Loader from '../../components/Loader'
import Swal from 'sweetalert2'
import Input from '../../components/Input'
import { initialFormState, idFromPath, currentInstitution } from '../../utils'
import { StylesManager } from 'survey-creator'

export const url = process.env.REACT_APP_IMAGE_STORAGE_URL

const User = props => {

  const fields = [
    { name: 'name', required: true },
    { name: 'subdomain', required: true },
    { name: 'description', required: true },
    { name: 'city', required: true },
    { name: 'street', required: true},
    { name: 'post_code', required: true },
    { name: 'email', required: true },
    { name: 'phone', required: true },
    { name: 'template_id', required: true},
    { name: 'images', required: true}
  ]

  const [loading, setLoading] = useState('hidden')
  const [form, setForm] = useState(initialFormState(fields))
  const [template, setTemplate] = useState({})
  const [logo, setLogo] = useState()
  const [image, setImage] = useState()

  useEffect(() => {
    const fetchInstitution = async () => {
      setLoading('loading')
      const res = await api().get(`/institutions/${props.match.params.id}`)
      if (res.status === 200) {
        setLoading('success')
        const data = res.data
        setForm({
          name :{ value: data.data.attributes.name, touched: false, required: true, errors: false },
          subdomain:{ value: data.data.attributes.subdomain, touched: false, required: true, errors: false },
          template_id:{ value: data.data.relationships.template.data.id, touched: false, required: true, errors: false },
          description :{ value: data.data.attributes.description, touched: false, required: false, errors: false },
          city :{ value: data.data.attributes.city, touched: false, required: true, errors: false },
          street :{ value: data.data.attributes.street, touched: false, required: false, errors: false },
          post_code :{ value: data.data.attributes.post_code, touched: false, required: false, errors: false },
          email :{ value: data.data.attributes.email, touched: false, required: false, errors: false },
          phone :{ value: data.data.attributes.phone, touched: false, required: false, errors: false },
          images: {value: data.included, touched: false, required: false, errors: false }
        })
      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchInstitution()
  }, [])

  useEffect(() => {
    if(!form.images.value) return
    if(!form.images.value.find(item => item.type == 'logo')) return
    const fetchLogo = async() => {
    const img = form.images.value.find(item => item.type == 'logo').attributes.small_url
    setLogo(img)
    }
    fetchLogo()
  },[form.images.value])

  useEffect(() => {
    if(!form.images.value) return
    if(!form.images.value.find(item => item.type == 'image')) return
    const fetchImage = async() => {
    const img = form.images.value.find(item => item.type == 'image').attributes.small_url
    setImage(img)
    }
    fetchImage()
  },[form.images.value])

  useEffect(() => {
    if(!form.template_id) return null
    const fetchTemplate = async () => {
      setLoading('loading')
      const id = form.template_id.value
      const res = await api().get(`/templates/${id}`)
      if (res.status === 200) {
        setLoading('success')
        const data = res.data.data
        setTemplate(data)
      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchTemplate()
  }, [form.template_id]) 


  const themeColor = (pcolor, scolor, name) => {
    return (
    <div style={{width: '100%', display: 'flex',flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
        <p style={{ width: 30, height: 30, borderRadius: 50, background: pcolor, marginRight: 10}}></p>
        <p style={{ width: 30, height: 30, borderRadius: 50, background: scolor, marginRight: 30}}></p>
        <p >{name}</p>
      </div>
    )
  }

  const logoShow = (
    <Col lg="6">
      <label className="form-control-label">Logo</label>
      <br/>
      <img style={styles.image} src={`${url}${logo}`}/>
    </Col>
  )

  const imgShow = (
    <Col lg="6">
      <label className="form-control-label">Zdjęcie</label>
      <br/>
      <img style={styles.image} src={`${url}${image}`}/>
  </Col>
  )

  const images = (
    <div className="pl-lg-4">
      <Row>
        {logo ? logoShow : null}
        {image ? imgShow : null}
      </Row>
      {(logo || image) ? <hr className="my-4" /> : null}
    </div>
  )


  return (
    <ContentWrapper title="Pokaż gminę">
      <Loader status={loading} />
      <Card className="bg-secondary shadow">
        <CardBody>
          <Form>
            <h6 className="heading-small text-muted mb-4">
              Informacje o gminie
            </h6>
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  <Input
                    title="Nazwa"
                    placeholder="Nazwa"
                    data={form.name}
                    name='name'
                    disabled
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Subdomena"
                    placeholder="Subdomena"
                    data={form.subdomain}
                    name='subdomain'
                    disabled
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="6">
                  <FormGroup>
                    <label className="form-control-label">
                      Motyw
                    </label>
                    <br />
                  {template.id === form.template_id.value ? themeColor(template.attributes.primary_color, template.attributes.secondary_color, template.attributes.name ) : null}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col lg="6">
                  <FormGroup>
                    <label className="form-control-label">
                      Opis
                    </label>
                    <br />
                    {form.description.value}
                  </FormGroup>
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <h6 className="heading-small text-muted mb-4">
              Dane kontaktowe
            </h6>
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  <Input
                    title="Miejscowość"
                    disabled
                    placeholder="Miejscowość"
                    data={form.city}
                    name='city'
                    type="text"
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Ulica"
                    disabled
                    placeholder="Ulica"
                    data={form.street}
                    name='street'
                    type="text"
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="6">
                  <Input
                    title="Kod pocztowy"
                    disabled
                    placeholder="Kod pocztowy"
                    data={form.post_code}
                    name='post_code'
                    type="text"
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Email"
                    disabled
                    placeholder="Email"
                    data={form.email}
                    name='email'
                    type="email"
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="6">
                  <Input
                    title="Telefon"
                    disabled
                    placeholder="Telefon"
                    data={form.phone}
                    name='phone'
                    type="number"
                  />
                </Col>
              </Row>
              <hr className="my-4" />
              </div>
              {images}
            <Button
              color='primary'
              onClick={() => props.history.push(`/${currentInstitution()}/admin/institutions/${props.match.params.id}/edit`)}
            >
              Edytuj
            </Button>
          </Form>
        </CardBody>
      </Card>
    </ContentWrapper>
  )
}

const styles = {
  image: {
    maxHeight: 400
  }
}


export default User
