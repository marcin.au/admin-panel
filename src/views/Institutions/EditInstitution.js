/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect } from 'react'
import ContentWrapper from '../../components/ContentWrapper'
import { Card, Row, Col, Button, CardBody, Form, Label } from 'reactstrap'
import api from 'api'
import Loader from '../../components/Loader'
import Swal from 'sweetalert2'
import Input from '../../components/Input'
import { initialFormState, formPayload, parseErrors, idFromPath, currentInstitution } from '../../utils'
import validate from './Validate'
import Select, {components}  from 'react-select'
import Dropdown from '../../components/Dropdown'
import * as request from '../../components/deleteImage'
import './style.css'

export const url = process.env.REACT_APP_IMAGE_STORAGE_URL

const User = props => {

  const fields = [
    { name: 'name', required: true },
    { name: 'subdomain', required: true },
    { name: 'description', required: false },
    { name: 'city', required: true },
    { name: 'street', required: false},
    { name: 'post_code', required: false },
    { name: 'email', required: false },
    { name: 'phone', required: false },
    { name: 'template_id', required: true},
    { name: 'images', required: true}
  ]

  const [loading, setLoading] = useState('hidden')
  const [form, setForm] = useState(initialFormState(fields))
  const [validation, setValidation] = useState(false)
  const [FieldsTouched, setFieldsTouched] = useState(false)
  const [template, setTemplate] = useState([])
  const [logo, setLogo] = useState()
  const [image, setImage] = useState()
  const [logoAvailable, setLogoAvailable] = useState()
  const [imageAvailable, setImageAvailable] = useState()
  const [logoDel, setLogoDel] = useState(true)
  const [imgDel, setImgDel] = useState(true)

  useEffect(() => {
    const validateAll = () => {
      for (let field of fields) {
        if (form[field.name].errors) {
          return false
        }
      }
      return true
    }
    setValidation(validateAll())
  }, [fields, form])
  
  useEffect(() => {
    Object.keys(form).forEach(name => {
      const errors = validate(name, form[name].value, form)
      if (form[name].errors !== errors) {
        setForm(form => ({ ...form, [name]: { ...form[name], errors } }))
      }
    })
  }, [
    form.name.value,
    form.subdomain.value,
    form.description.value,
    form.city.value,
    form.street.value,
    form.post_code.value,
    form.email.value,
    form.phone.value,
    form.template_id.value
  ])

  useEffect(() => {
    const fetchInstitution = async () => {
      setLoading('loading')
      const res = await api().get(`/institutions/${props.match.params.id}`)
      if (res.status === 200) {
        setLoading('success')
        const data = res.data
        console.log(data)
        setForm({
          name :{ value: data.data.attributes.name, touched: false, required: true, errors: false },
          subdomain:{ value: data.data.attributes.subdomain, touched: false, required: true, errors: false },
          template_id:{ value: data.data.relationships.template.data.id, touched: false, required: true, errors: false },
          description :{ value: data.data.attributes.description, touched: false, required: false, errors: false },
          city :{ value: data.data.attributes.city, touched: false, required: true, errors: false },
          street :{ value: data.data.attributes.street, touched: false, required: false, errors: false },
          post_code :{ value: data.data.attributes.post_code, touched: false, required: false, errors: false },
          email :{ value: data.data.attributes.email, touched: false, required: false, errors: false },
          phone :{ value: data.data.attributes.phone, touched: false, required: false, errors: false },
          images: {value: data.included, touched: false, required: false, errors: false }
        })
      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchInstitution()
  }, [])

  useEffect(() => {
    if(!form.images.value) return
    if(!form.images.value.find(item => item.type == 'logo')) return
    const fetchLogo = async() => {
    const img = form.images.value.find(item => item.type == 'logo').attributes.small_url
    console.log(img)
    setLogo(img)
    setLogoAvailable(true)
    }
    fetchLogo()
  },[form.images.value])

  useEffect(() => {
    if(!form.images.value) return
    if(!form.images.value.find(item => item.type == 'image')) return
    const fetchImage = async() => {
    const img = form.images.value.find(item => item.type == 'image').attributes.small_url
    setImage(img)
    setImageAvailable(true)
    }
    fetchImage()
  },[form.images.value])

  const updateInstitution = async () => {
    console.log(form)
    setLoading('loading')
    const upload_data = new FormData()
    for(let key in form){ upload_data.append(`institution[${key}]`, form[key].value)}
    upload_data.delete('institution[images]')
    if(typeof(logo) != 'string'){upload_data.append('institution[logo]', logo)}
    if(typeof(image) != 'string'){upload_data.append('institution[images][]', image)}
    const res = await api().put(`/institutions/${props.match.params.id}`, upload_data)
    if (res.status === 200) {
      setLoading('success')
      Swal.fire({
        title: 'Gmina zaktualizowana',
        icon: 'success',
        timer: 1000,
        showConfirmButton: false,
        timerProgressBar: true,
      })
      props.history.push(`/${currentInstitution()}/admin/institutions`)
    } else {
      console.log(res.data.data)
      setLoading('error')
      Swal.fire({
        title: 'Wystąpiły błedy',
        // html: parseErrors(res.data.data.errors),
        icon: 'error'
      })
    }
  }

  useEffect(() => {
    const fetchTemplate = async () => {
      setLoading('loading')
      const res = await api().get(`/templates`)
      console.log(res.data.data)
      if (res.status === 200) {
        setLoading('success')
        const data = res.data.data
        setForm(form => ({
          ...form,
          template_id: { ...form.template_id, value: data[0].id, }
        }))
        setTemplate(data.map(item => ({value: item.id, name: item.attributes.name, label: item.attributes.name, primary_color: item.attributes.primary_color, secondary_color: item.attributes.secondary_color})))
      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchTemplate()
  }, []) 

  const updateForm = (event) => {
    const { name, value } = event.target;

    if (!FieldsTouched) {
      setFieldsTouched(true)
    }

    if (!form[name].touched) {
      setForm(form => ({ ...form, [name]: { ...form[name], touched: true } }))
    }

    if (form[name].value !== value) {
      setForm(form => ({ ...form, [name]: { ...form[name], value } }))
    }
  }

  const addTemplateHandler = (selectValue) => {
    if (!FieldsTouched) {
      setFieldsTouched(true)
    }
    setForm({...form, 'template_id': {value: selectValue.value, touched: true}})
  }

  const deleteLogo = async () => {
    const del = await request.destroy(`/institutions/${props.match.params.id}/logo`, 'Logo')
    if(del === 204){
      setLogoDel(false)
    }
  }

  const deleteImage = async () => {
    const del = await request.destroy(`/institutions/${props.match.params.id}/image`, 'Zdjęcie')
    if(del === 204){
      setImgDel(false)
    }
  }

  const themeColor = (color, s_color, name) => {
    return <div style={{width: '100%', display: 'flex',flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
        <p style={{width: 20, height: 20, borderRadius: 50, background: color, marginRight: 10}}></p>
        <p style={{width: 20, height: 20, borderRadius: 50, background: s_color, marginRight: 30}}></p>
        <p >{name}</p>
      </div>
  }

const { Option, SingleValue } = components;
const ColorOption = props => (
  <Option {...props}>
    {themeColor(props.data.primary_color, props.data.secondary_color, props.data.name)}
  </Option>
);

const SingleOption = props => (
  <SingleValue {...props}>
    {themeColor(props.data.primary_color, props.data.secondary_color, props.data.name )}
  </SingleValue>
);

  const setThemeHandler = (
    <Col lg="12">
      <label className="form-control-label">
        Wybierz motyw
      </label>
      <Select
        name="template_id"
        onChange={addTemplateHandler}
        data={form.template_id}
        value={template[form.template_id.value - 1 ]}
        options={template}
        components={{ Option: ColorOption, SingleValue: SingleOption }}
        styles={{control: styles => ({...styles, marginBottom: 20, width: "100%"})}}
        placeholder="Wybierz kolor..."
      />
    </Col>
)

  return (
    <ContentWrapper title="Edytuj gminę">
      <Loader status={loading} />
      <Card className="bg-secondary shadow">
        <CardBody>
          <Form>
            <h6 className="heading-small text-muted mb-4">
              Informacje o gminie
            </h6>
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  <Input
                    title="Nazwa gminy"
                    placeholder="Name"
                    data={form.name}
                    name='name'
                    onChange={updateForm}
                    type="text"
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Subdomena"
                    placeholder="Subdomena"
                    data={form.subdomain}
                    name='subdomain'
                    onChange={updateForm}
                    type="text"
                  />
                </Col>
              </Row>
              <Row>
                <Col lg='12'>
                  {setThemeHandler}
                </Col>
              </Row>
              <Row>
                <Col lg="12">
                  <Input
                    title="Opis"
                    placeholder="Opis"
                    data={form.description}
                    name='description'
                    rows="5"
                    onChange={updateForm}
                    type="textarea"
                  />
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <h6 className="heading-small text-muted mb-4">
              Dane kontaktowe
            </h6>
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  <Input
                    title="Miejscowość"
                    onChange={updateForm}
                    placeholder="Miejscowość"
                    data={form.city}
                    name='city'
                    type="text"
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Ulica"
                    onChange={updateForm}
                    placeholder="Ulica"
                    data={form.street}
                    name='street'
                    type="text"
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="6">
                  <Input
                    title="Kod pocztowy"
                    onChange={updateForm}
                    placeholder="Kod pocztowy"
                    data={form.post_code}
                    name='post_code'
                    type="text"
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Email"
                    onChange={updateForm}
                    placeholder="Email"
                    data={form.email}
                    name='email'
                    type="email"
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="6">
                  <Input
                    title="Telefon"
                    onChange={updateForm}
                    placeholder="Telefon"
                    data={form.phone}
                    name='phone'
                    type="number"
                  />
                </Col>
                </Row>
            </div>    
            <hr className="my-4" />
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                    <Label for="form-control-label">Logo</Label>
                    <Dropdown setImage={setLogo} imgAvailable={logoAvailable} imageUrl={`${url}${logo}`} deleteImg={deleteLogo} isDeleted={logoDel}/>
                </Col>
                <Col lg="6">
                  <Label for="form-control-label">Zdjęcie</Label>
                  <Dropdown setImage={setImage} imgAvailable={imageAvailable} imageUrl={`${url}${image}`} deleteImg={deleteImage} isDeleted={imgDel}/>
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <Button
              color={validation && FieldsTouched ? 'success' : 'danger'}
              onClick={updateInstitution}
              disabled={!validation || !FieldsTouched}
            >
              Aktualizuj
            </Button>
          </Form>
        </CardBody>
      </Card>
    </ContentWrapper>
  )
}

export default User
