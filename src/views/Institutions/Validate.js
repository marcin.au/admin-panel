export default (name, value, form) => {
  let errors = []
  if (!form[name].touched) return

  switch (name) {
    case 'name':
      if (value === '') {
        errors.push('Nazwa jest pusta')
      }
      break
    case 'subdomain':
      if (value === '') {
        errors.push('Subdomena jest pusta')
      }
      break
    case 'email':
      const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      if (!emailRegex.test(String(value).toLocaleLowerCase()) && value !== '') {
        errors.push('Email jest nie poprawny')
      }
      break
    case 'post_code':
      const postCodeRegex = /[0-9]{2}-[0-9]{3}/
      if (!postCodeRegex.test(String(value).toLocaleLowerCase()) && value !== '') {
        errors.push('Kod pocztowy jest nie poprawny')
      }
      break
    default:
      break
  }

  return errors.length === 0 ? false : errors
}