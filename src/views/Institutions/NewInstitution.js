/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect, useRef, useCallback } from 'react'
import ContentWrapper from '../../components/ContentWrapper'
import { Card, Row, Col, Button, CardBody, Form, FormGroup, Label, CustomInput } from 'reactstrap'
import api from 'api'
import Loader from '../../components/Loader'
import Swal from 'sweetalert2'
import Input from '../../components/Input'
import { initialFormState, formPayload, parseErrors, currentInstitution } from '../../utils'
import validate from './Validate'
import Select, {components}  from 'react-select'
import { useDropzone } from 'react-dropzone'
import Dropdown from '../../components/Dropdown'


const User = props => {

  const fields = [
    { name: 'name', required: true },
    { name: 'subdomain', required: true },
    { name: 'description', required: false },
    { name: 'city', required: true },
    { name: 'street', required: false},
    { name: 'post_code', required: false },
    { name: 'email', required: false },
    { name: 'phone', required: false },
    { name: 'template_id', required: true}
  ]

  const [loading, setLoading] = useState('hidden')
  const [form, setForm] = useState(initialFormState(fields))
  const [validation, setValidation] = useState(false)
  const [template, setTemplate] = useState([])
  const [logo, setLogo] = useState()
  const [image, setImage] = useState()
  const [showLogo, setShowLogo] = useState()
  let logo1 = useRef()

  const styles = {
    themeColorBox: {
      width: '100%', 
      display: 'flex',
      flexDirection: 'row', 
      alignItems: 'center', 
      marginTop: 10
    },
    primaryColor: {
      width: 20, 
      height: 20, 
      borderRadius: 50, 
      marginRight: 10
    },
    secondrayColor: {
      width: 20, 
      height: 20, 
      borderRadius: 50,  
      marginRight: 30
    },
    dropzone: {
      display: 'flex',
      justifyContent: "center",
      alignItems: "center",
      border: 'dashed 2px #ccc',
      width: '100%',
      backgroundColor: '#efefef',
      height: 150,
      borderRadius: 4,
      marginBottom: 10,
    }
  }


  useEffect(() => {
    const validateAll = () => {
      for (let field of fields) {
        if ((form[field.name].errors) || (field.required && !form[field.name].touched)) {
          return false
        }
      }
      return true
    }
    setValidation(validateAll())
  }, [fields, form])

  useEffect(() => {
    Object.keys(form).forEach(name => {
      const errors = validate(name, form[name].value, form)
      console.log(errors)
      if (form[name].errors !== errors) {
        setForm(form => ({ ...form, [name]: { ...form[name], errors } }))
      }
    })
  }, [
    form.name.value,
    form.subdomain.value,
    form.description.value,
    form.city.value,
    form.street.value,
    form.post_code.value,
    form.email.value,
    form.phone.value,
    form.template_id.value
  ])

  // const fileSelectedHandler = (e) => {
  //   if(!e.target.files[0]) return
  //   setLogo(e.target.files[0])
  //   console.log(e.target.files[0])
  //   let reader = new FileReader()
  //   reader.readAsDataURL(e.target.files[0])
  //   reader.onload = () => {setShowLogo(reader.result)}
  // }


  // const test = () => {
  //   const upload_data = new FormData()
  //   for(let key in form){ upload_data.append(key, form[key].value)}
  //   upload_data.append('logo', logo)
  //   for(let key of upload_data.entries()){
  // console.log(key)
  //   }
  // console.log(logo1.value)
  // }


  const createInstitution = async () => {
    setLoading('loading')
    const upload_data = new FormData()
    for(let key in form){ upload_data.append(`institution[${key}]`, form[key].value)}
    if(logo){upload_data.append('institution[logo]', logo)}
    if(image){upload_data.append('institution[images][]', image)}
    const res = await api().post('/institutions', upload_data)
    if (res.status === 201) {
      setLoading('success')
      console.log(res)
      Swal.fire({
        title: 'Gmina utworzona',
        icon: 'success',
        timer: 1000,
        showConfirmButton: false,
        timerProgressBar: true,
      })
      props.history.push(`/${currentInstitution}/admin/institutions`)
    } else {
      console.log(res)
      setLoading('error')
      Swal.fire({
        title: 'Wystąpiły błedy',
        //html: parseErrors(res?.data?.data?.errors),
        icon: 'error'
      })
    }
  }

  useEffect(() => {
    const fetchTemplate = async () => {
      setLoading('loading')
      const res = await api().get(`/templates`)
      console.log(res.data.data)
      if (res.status === 200) {
        setLoading('success')
        const data = res.data.data
        setForm(form => ({
          ...form,
          template_id: { ...form.template_id, value: data[0].id, }
        }))
        setTemplate(data.map(item => ({value: item.id, name: item.attributes.name, label: item.attributes.name, primary_color: item.attributes.primary_color, secondary_color: item.attributes.secondary_color})))
      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchTemplate()
  }, []) 

  console.log(template)
  const updateForm = (event) => {
    const { name, value } = event.target;
    console.log(form)

    if (!form[name].touched) {
      setForm(form => ({ ...form, [name]: { ...form[name], touched: true } }))
    }

    if (form[name].value !== value) {
      setForm(form => ({ ...form, [name]: { ...form[name], value } }))
    }
  }

  const addTemplateHandler = (selectValue) => {
    setForm({...form, 'template_id': {value: selectValue.value, touched: true}})
  }

  const themeColor = (color, s_color, name) => {
    return <div style={styles.themeColorBox}>
        <p style={{...styles.primaryColor, background: color}}></p>
        <p style={{...styles.secondrayColor, background: s_color}}></p>
        <p >{name}</p>
      </div>
  }

const { Option, SingleValue } = components;
const ColorOption = props => (
  <Option {...props}>
    {themeColor(props.data.primary_color, props.data.secondary_color, props.data.name)}
  </Option>
);

const SingleOption = props => (
  <SingleValue {...props}>
    {themeColor(props.data.primary_color, props.data.secondary_color, props.data.name )}
  </SingleValue>
);

const setThemeHandler = (
  <Col lg="12">
    <label className="form-control-label">
      Wybierz motyw
    </label>
    <Select
      name="template_id"
      defaultValue={template[0]}
      onChange={addTemplateHandler}
      data={form.template_id}
      value={template.id}
      options={template}
      components={{ Option: ColorOption, SingleValue: SingleOption }}
      styles={{control: styles => ({...styles, marginBottom: 20, width: "100%"})}}
    />
  </Col>
)

// const onDrop = useCallback(acceptedFiles => {
//   let reader = new FileReader()
//     reader.readAsDataURL(acceptedFiles[0])
//     reader.onload = () => {setShowLogo(reader.result)}
// }, [])

// const {getRootProps, getInputProps, acceptedFiles, acceptedFiles2} = useDropzone({onDrop})

// const files = acceptedFiles.map(file => (
//   <li key={file.path}>
//     {file.path} - {file.size} bytes
//   </li>
// ));

const test2 = () => {
  console.log(logo)
  console.log(image)
}


// const dropzone = (
//   <section className="container">
//   <div {...getRootProps()} style={styles.dropzone}>
//     <input {...getInputProps()} />
//     <p>Drag 'n' drop some files here, or click to select files</p>
//   </div>
//   <aside>
//     <h4>Files</h4>
//     <ul>{files}</ul>
//   </aside>
// </section>
// )

const deleteImage = () => {
  setLogo()
  console.log(logo1.value)
  logo1.value = null
}

// const logoView = (logo) => {
//     if(logo){
//       return (
//         <div style={styles.imageBox}>
//           <div style={styles.deleteImage} onClick={()=>{deleteImage()}}>X</div>
//         </div>
//       )
//     }
//   }


  return (
    <ContentWrapper title="Dodaj gminę">
      <Loader status={loading} />
      <Card className="bg-secondary shadow">
        <CardBody>
          <Form>
            <h6 className="heading-small text-muted mb-4">
              Główne informacje
            </h6>
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  <Input
                    title="Nazwa gminy"
                    placeholder="Name"
                    data={form.name}
                    name='name'
                    onChange={updateForm}
                    type="text"
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Subdomena"
                    placeholder="Subdomena"
                    data={form.subdomain}
                    name='subdomain'
                    onChange={updateForm}
                    type="text"
                  />
                </Col>
              </Row>
              <Row>
                <Col lg='12'>
                  {setThemeHandler}
                </Col>
              </Row>
              <Row>
                <Col lg="12">
                  <Input
                    title="Opis"
                    placeholder="Opis"
                    data={form.description}
                    name='description'
                    rows="5"
                    onChange={updateForm}
                    type="textarea"
                  />
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <h6 className="heading-small text-muted mb-4">
              Dane kontaktowe
            </h6>
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  <Input
                    title="Miejscowość"
                    placeholder="Miejscowość"
                    data={form.city}
                    name='city'
                    onChange={updateForm}
                    type="text"
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Ulica"
                    placeholder="Ulica"
                    data={form.street}
                    name='street'
                    onChange={updateForm}
                    type="text"
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="6">
                  <Input
                    title="Kod pocztowy"
                    placeholder="Kod pocztowy"
                    data={form.post_code}
                    name='post_code'
                    onChange={updateForm}
                    type="text"
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Email"
                    placeholder="Email"
                    data={form.email}
                    name='email'
                    onChange={updateForm}
                    type="email"
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="6">
                  <Input
                    title="Telefon"
                    placeholder="Telefon"
                    data={form.phone}
                    name='phone'
                    onChange={updateForm}
                    type="number"
                  />
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  {/* <Form>
                    <label className="form-control-label">
                      Dodaj zdjęcie
                    </label>
                    <div className="custom-file">
                      <input
                        className="custom-file-input"
                        id="customFileLang"
                        lang="pl"
                        type="file"
                        style={{ cursor: 'pointer' }}
                        accept='.jpg, .gif, .png'
                        ref={logo}
                      />
                      <label
                        className="custom-file-label"
                        htmlFor="customFileLang"
                        style={{ cursor: 'pointer' }}
                      >
                        {}
                      </label>
                    </div>
                  </Form> */}
                    <Label for="form-control-label">Dodaj logo</Label>
                    {/* <div style={{display: 'flex', flexDirection: "row", alignItems: "center"}}> */}
                      {/* {dropzone} */}
                      {/* <CustomInput type="file" accept='.jpg, .gif, .png' onChange={fileSelectedHandler} innerRef={fileInput => logo1 = fileInput} id="exampleCustomFileBrowser" name="customFile" label="Wybierz zdjęcie..." style={{ cursor: 'pointer' }} />  */}
                      {/* {logoView(acceptedFiles[0])} */}
                    {/* </div> */}
                    <Dropdown setImage={setLogo}/>
                </Col>
                <Col lg="6">
                  <Label for="form-control-label">Dodaj zdjęcie</Label>
                  <Dropdown setImage={setImage}/>
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <Button
              color={validation ? 'success' : 'danger'}
              onClick={createInstitution}
              disabled={!validation}
            >
              Dodaj
            </Button>
            <Button
              onClick={test2}
            >
              Dodaj
            </Button>
          </Form>
        </CardBody>
      </Card>
    </ContentWrapper>
  )
}

export default User
