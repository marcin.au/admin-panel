
import React, { useState } from "react"
import Actions from '../../components/Actions'
import Paginator from '../../components/Paginator'
import { withRouter, Link } from 'react-router-dom'
import ContentWrapper from '../../components/ContentWrapper'
import ContentTable from '../../components/ContentTable'
import * as requests from '../../requests'
import Colors from '../../assets/variables/Colors'
import { parseDate, currentInstitution } from '../../utils'
import { StylesManager } from "survey-creator"

export const url = process.env.REACT_APP_IMAGE_STORAGE_URL

const Institutions = props => {

  const [institutions, setInstitutions] = useState([])

  const showHandler = id => {
    props.history.push(`/${currentInstitution()}/admin/institutions/${id}`)
  }

  const editHandler = id => {
    props.history.push(`/${currentInstitution()}/admin/institutions/${id}/edit`)
  }

  const deleteHandler = id => {
    const callback = () => setInstitutions(institutions => institutions.filter(item => item.id !== id))
    requests.destroy(`/institutions/${id}`, callback)
  }

  const getDate = (date) => {
    return date ? parseDate(date) : <span style={{color: Colors.orange}}>Brak daty</span>
  }

  const active = <b style={{ color: Colors.green }}>Aktywny</b>
  const inactive = <b style={{ color: Colors.red }}>Nie aktywny</b>

  const institutionsJSX = institutions.map(item => (
    <tr key={item.id}>
      <td><Link to={`/${currentInstitution()}/admin/institutions/${item.id}`}>{item.id}</Link></td>
      <td><Link to={`/${currentInstitution()}/admin/institutions/${item.id}`}><img src={`${url}${item.attributes.logo_url}`} style={styles.logo} alt=""/>{item.attributes.name}</Link></td>
      <td>{item.attributes.subdomain}</td>
      <td>{item.attributes.active ? active : inactive}</td>
      <td>{getDate(item.attributes.updated_at)}</td>
      <td>
        <Actions
          showHandler={() => showHandler(item.id)}
          deleteHandler={() => deleteHandler(item.id)}
          editHandler={() => editHandler(item.id)}
        />
      </td>
    </tr>
  ))

  const headers = 'Id|Nazwa|Subdomena|Status|Ostatnia modyfikacja|Akcje'.split('|')

  return (
    <ContentWrapper title='Gminy'>
      <ContentTable headers={headers}>
        {institutionsJSX}
      </ContentTable>
      <Paginator
        assetName='institutions'
        setCollection={setInstitutions}
        skipInstitution
      />
    </ContentWrapper>
  )
}


const styles = {
  logo: {
    maxWidth: 50,
    marginRight: 15
  }
}

export default withRouter(Institutions)
