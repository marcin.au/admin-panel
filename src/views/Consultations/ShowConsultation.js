/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect } from 'react'
import ContentWrapper from '../../components/ContentWrapper'
import { Card, Row, Col, Button, CardBody, Form, FormGroup } from 'reactstrap'
import api from 'api'
import Loader from '../../components/Loader'
import Swal from 'sweetalert2'
import Input from '../../components/Input'
import { initialFormState, idFromPath, parseDate, currentInstitution } from '../../utils'
import Survey from '../../components/Survey'
import ReactHtmlParser from 'react-html-parser'

const votingMethodTranslation = {
  open: 'Otwarta',
  pesel: 'Pesel',
  pesel_whitelist: 'Wybrane pesele'
}

const User = props => {

  const fields = [
    { name: 'begin_at', required: true },
    { name: 'end_at', required: true },
    { name: 'long_description', required: true },
    { name: 'published_at', required: true },
    { name: 'short_description', required: true },
    // { name: 'survey', required: false },
    { name: 'title', required: true },
    { name: 'category_id', required: true },
    { name: 'institution_id', required: true },
    { name: 'body', required: true },
    { name: 'voting_method', required: true }
  ]

  const [loading, setLoading] = useState('hidden')
  const [form, setForm] = useState(initialFormState(fields))
  const [survey, setSurvey] = useState(null)
  const [subdomain, setSubdomain] = useState('')

  useEffect(() => {
    const fetchInstitution = async () => {
      setLoading('loading')
      const res = await api({ withInstitution: true }).get(`/consultations/${props.match.params.id}`)
      if (res.status === 200) {
        setLoading('success')
        const data = res.data.data
        const included = res.data.included
        console.log(res.data)
        console.log('dfgdfg')
        setSubdomain(included[1].attributes.subdomain)
        setForm({
          begin_at: { value: data.attributes.begin_at, touched: false, required: true, errors: false}, 
          end_at: { value: data.attributes.end_at, touched: false, required: true, errors: false}, 
          long_description: { value: data.attributes.long_description, touched: false, required: true, errors: false}, 
          short_description: { value: data.attributes.short_description, touched: false, required: true, errors: false}, 
          title: { value: data.attributes.title, touched: false, required: true, errors: false}, 
          body: { value: data.attributes.body, touched: false, required: false, errors: false},
          category_id: { value: included[0].attributes.name, touched: false, required: true, errors: false}, 
          institution_id: { value: included[1].attributes.name, touched: false, required: true, errors: false},
          published_at: { value: data.attributes.published_at, touched: false, required: true, errors: false},
          voting_method: { value: votingMethodTranslation[data.attributes.voting_method], touched: false, required: true, errors: false}
        })
        setSurvey(JSON.parse(data.attributes.survey) ?? {})
      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchInstitution()
  }, [])

  const goToConsultation = () => {
    const slug = props.match.params.id
    window.location.replace(`https://consultations-media-park.web.app/${subdomain}/${slug}`)
  }

  return (
    <ContentWrapper title="Pokaż Konsultacje">
      <Loader status={loading} />
      <Card className="bg-secondary shadow">
        <CardBody>
          <Form>
            <h6 className="heading-small text-muted mb-4">
              Informacje o konsultacji
            </h6>
            <div className="pl-lg-4">
              <Row>
                <Col lg="12">
                  <Input
                    title="Tytuł"
                    placeholder="Tytuł"
                    data={form.title}
                    name='title'
                    disabled
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="4">
                <FormGroup>
                    <label className="form-control-label">
                      Data publikacji
                    </label>
                    <br />
                    {parseDate(form.published_at.value)}
                  </FormGroup>
                </Col>
                <Col lg="4">
                <FormGroup>
                    <label className="form-control-label">
                      Czas rozpoczęcia
                    </label>
                    <br />
                    {parseDate(form.begin_at.value)}
                  </FormGroup>
                </Col>
                <Col lg="4">
                <FormGroup>
                    <label className="form-control-label">
                      Czas zakończenia
                    </label>
                    <br />
                    {parseDate(form.end_at.value)}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
              <Col lg="12">
                <label className='form-control-label'>Krótki opis</label>
                <br/>
                <div style={{padding: 10, border: 'solid 1px #ccc', marginBottom: 10}}>
                  {ReactHtmlParser(form.short_description.value)}
                </div>
              </Col>
            </Row>
            <Row>
              <Col lg="12">
                <label className='form-control-label'>Długi opis</label>
                <br/>
                <div style={{padding: 10, border: 'solid 1px #ccc', marginBottom: 10}}>
                  {ReactHtmlParser(form.long_description.value)}
                </div>
              </Col>
            </Row>
            <Row>
              <Col lg="12">
                <label className='form-control-label'>Ciało</label>
                <br/>
                <div style={{padding: 10, border: 'solid 1px #ccc', marginBottom: 10}}>
                  {ReactHtmlParser(form.body.value)}
                </div>
              </Col>
            </Row>
              <Row>
                <Col lg="6">
                  <Input
                      title="Kategoria"
                      placeholder="Kategoria"
                      data={form.category_id}
                      name='category_id'
                      disabled
                    />
                </Col>
                <Col lg="6">
                <Input
                      title="Gmina"
                      placeholder="Gmina"
                      data={form.institution_id}
                      name='institution_id'
                      disabled
                    />
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <h6 className="heading-small text-muted mb-4">
              Ankieta
            </h6>
            <Row>
              <Col lg="12">
                <Survey
                  value={survey}
                  onChange={value => setSurvey(value)}
                  mode='show'
                />
              </Col>
            </Row>
            <hr className="my-4" />
            <Row>
              <Col lg="6">
                <Input
                  title="Metoda głosowania"
                  placeholder="Metoda głosowania"
                  data={form.voting_method}
                  name='voting_method'
                  disabled
                />
              </Col>
            </Row>
            <hr className="my-4" />
            <Button
              color='primary'
              onClick={() => props.history.push(`/${currentInstitution()}/admin/consultations/${props.match.params.id}/edit`)}
            >
              Edytuj
            </Button>
            <Button color='primary' onClick={goToConsultation}>
              Przejdź do konsultacji
            </Button>
          </Form>
        </CardBody>
      </Card>
    </ContentWrapper>
  )
}


export default User
