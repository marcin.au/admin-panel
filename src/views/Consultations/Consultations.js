
import React, { useState, useEffect } from "react"
import Actions from '../../components/Actions'
import Paginator from '../../components/Paginator'
import { withRouter, Link } from 'react-router-dom'
import ContentWrapper from '../../components/ContentWrapper'
import ContentTable from '../../components/ContentTable'
import * as requests from '../../requests'
import Colors from '../../assets/variables/Colors'
import { parseDate } from '../../utils'
import { currentInstitution } from '../../utils'

const Consultations = props => {
  const [consultations, setConsultations] = useState([])

  const showHandler = id => {
    props.history.push(`/${currentInstitution()}/admin/consultations/${id}`)
  }

  const editHandler = id => {
    props.history.push(`/${currentInstitution()}/admin/consultations/${id}/edit`)
  }

  const deleteHandler = id => {
    const callback = () => setConsultations(consultations => consultations.filter(item => item.id !== id))
    requests.destroy(`institutions/${currentInstitution()}/consultations/${id}`, callback)
  }

  const getDate = (date) => {
    return date ? parseDate(date) : <span style={{color: Colors.grey}}>Brak daty</span>
  }

  const status = {
    active: <b style={{ color: Colors.blue }}>Aktywna</b>,
    inactive: <b style={{ color: Colors.red }}>Nieaktywna</b>,
    draft: <b style={{ color: Colors.orange }}>Kopia robocza</b>,
    finished: <b style={{ color: Colors.green }}>Ukończona</b>,
  }

  const consultationsJSX = consultations.map(item => (
    <tr key={item.id}>
      <td><Link to={`/${currentInstitution()}/admin/consultations/${item.id}`}>{item.attributes.title}</Link></td>
      <td>{getDate(item.attributes.begin_at)}</td>
      <td>{getDate(item.attributes.end_at)}</td>
      <td>{status[item.attributes.status]}</td>
      <td>{getDate(item.attributes.updated_at)}</td>
      <td>
        <Actions
          showHandler={() => showHandler(item.id)}
          deleteHandler={() => deleteHandler(item.id)}
          editHandler={() => editHandler(item.id)}
        />
      </td>
    </tr>
  ))

  const headers = 'Nazwa|Data rozpoczęcia|Data zakończenia|Status|Ostatnia modyfikacja|Akcje'.split('|')

  return (
    <ContentWrapper title='Konsultacje'>
      <ContentTable headers={headers}>
        {consultationsJSX}
      </ContentTable>
      <Paginator
        assetName='consultations'
        setCollection={setConsultations}
      />
    </ContentWrapper>
  )
}

export default withRouter(Consultations)
