/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect } from 'react'
import ContentWrapper from '../../components/ContentWrapper'
import { Card, Row, Col, Button, CardBody, Form, CardHeader, ListGroup, ListGroupItem } from 'reactstrap'
import api from 'api'
import Loader from '../../components/Loader'
import Swal from 'sweetalert2'
import Input from '../../components/Input'
import { initialFormState, formPayload, parseErrors, getLocaDate, parseDate } from '../../utils'
import validate from './Validate'
import moment from 'moment'
import Survey from '../../components/Survey'
import DateTimePicker from 'react-datetime'
import { currentInstitution } from '../../utils'
import { Editor } from '@tinymce/tinymce-react';
import VotingMethods from '../../components/VotingMethods/Container'

const User = props => {

  const fields = [
    { name: 'long_description', required: true },
    { name: 'short_description', required: true },
    { name: 'title', required: true },
    { name: 'category_id', required: true, touched: true },
    { name: 'institution_id', required: true, touched: true },
    { name: 'body', required: true}
  ]

  const [loading, setLoading] = useState('hidden')
  const [form, setForm] = useState(initialFormState(fields))
  const [validation, setValidation] = useState(false)
  const [categories, setCategories] = useState([])
  const [institution, setInstitution] = useState([])
  const [survey, setSurvey] = useState({})
  const [beginDate, setBeginDate] = useState(null)
  const [endDate, setEndDate] = useState(null)
  const [publishedAt, setPublishedAt] = useState(moment().add(1, 'month'))
  const [votingMethod, setVotingMethod] = useState('open')

  const editorSettings = (height) => ({
    height: height,
    menubar: true,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table paste code help wordcount'
    ],
    toolbar:
      'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
  })
  
  useEffect(() => {
    const validateAll = () => {
      for (let field of fields) {
        if ((form[field.name].errors) || (field.required && !form[field.name].touched)) {
          return false
        }
      }
      return true
    }
    setValidation(validateAll())
  }, [fields, form])

  useEffect(() => {
    Object.keys(form).forEach(name => {
      const errors = validate(name, form[name].value, form)
      console.log(errors)
      if (form[name].errors !== errors) {
        setForm(form => ({ ...form, [name]: { ...form[name], errors } }))
      }
    })
  }, [
    form.short_description.value,
    form.long_description.value,
    form.title.value,
    form.category_id.value,
    form.institution_id.value,
    form.body.value
  ])

  const validateEndDate = (current) => {
    return current.isAfter(beginDate)
  }

  const validateBeginDate = (current) => {
    return current.isAfter(moment())
  }

  const validatePublishedAd = (current) => {
    return current.isAfter(moment())
  }

  const createConsultation = async () => {
    setLoading('loading')
    const res = await api({ withInstitution: true }).post('/consultations', { consultation: { 
      begin_at: beginDate ? beginDate.format() : null,
      end_at: endDate ? endDate.format() : null,
      long_description: form.long_description.value,
      published_at: publishedAt.format(),
      short_description: form.short_description.value,
      body: form.body.value,
      survey: JSON.stringify(survey ?? {}),
      title: form.title.value,
      category_id: form.category_id.value,
      institution_id: form.institution_id.value,
      voting_method: votingMethod,
      status: 'active'
    }})

    if (res.status === 201) {
      console.log(res.data)
      setLoading('success')
      Swal.fire({
        title: 'Konsultacja utworzona',
        icon: 'success',
        timer: 1000,
        showConfirmButton: false,
        timerProgressBar: true,
      })
      props.history.push(`/${currentInstitution()}/admin/consultations`)
    } else {
      setLoading('error')
      Swal.fire({
        title: 'Wystąpiły błedy',
        html: parseErrors(res?.data?.data?.errors),
        icon: 'error'
      })
    }
  }
  const setBodyHandler = (text) => {
    setForm({...form, 'body': {value: text, touched: true}})
  }

  const setShortDescriptionHandler = (text) => {
    setForm({...form, 'short_description': {value: text, touched: true}})
  }

  const setLongDescriptionHandler = (text) => {
    setForm({...form, 'long_description': {value: text, touched: true}})
  }

  const updateForm = (event) => {
    const { name, value } = event.target;

    if (!form[name].touched) {
      setForm(form => ({ ...form, [name]: { ...form[name], touched: true } }))
    }

    if (form[name].value !== value) {
      setForm(form => ({ ...form, [name]: { ...form[name], value } }))
    }
  }

  useEffect(() => {
    const fetchCategories = async () => {
      setLoading('loading')
      const res = await api().get(`/categories`)
      if (res.status === 200) {
        setLoading('success')
        const data = res.data.data
        setForm(form => ({
          ...form,
          category_id: { ...form.category_id, value: data[0].id, }
        }))
        setCategories(data)
      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchCategories()
  }, []) 

  useEffect(() => {
    const fetchIntitution = async () => {
      setLoading('loading')
      const res = await api().get(`/institutions`)
      if (res.status === 200) {
        setLoading('success')
        const data = res.data.data
        setForm(form => ({
          ...form,
          institution_id: { ...form.institution_id, value: data[0].id, }
        }))
        setInstitution(data)
      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchIntitution()
  }, []) 


  const setInstitutionHandler = (
    <Input type="select" title="Gmina" defaultValue="Wybierz role..." name='institution_id' onChange={updateForm} data={form.institution_id}>
      {institution.map(item => (<option value={item.id}>{item.attributes.name}</option>))}
    </Input>
  )


  const setICategoriesHandler = (
    <Input type="select" title="Kategoria" defaultValue="Wybierz role..." name='category_id' onChange={updateForm} data={form.category_id}>
      {categories.map(item => (<option value={item.id}>{item.attributes.name}</option>))}
    </Input>
  )



  return (
    <ContentWrapper title="Dodaj konsultacje">
      <Loader status={loading} />
      <Card className="bg-secondary shadow">
        <CardBody>
          <Form>
            <h6 className="heading-small text-muted mb-4">
              Informacje o konsultacji
            </h6>
            <div className="pl-lg-4">
            <Row>
                <Col lg="12">
                  <Input
                    title="Tytuł"
                    placeholder="Tytuł"
                    data={form.title}
                    name='title'
                    onChange={updateForm}
                  />
                </Col>
              </Row>
              <Row>
              <Col lg="4" style={{ marginBottom: 20 }}>
                  <label className='form-control-label'>Data publikacji</label>
                  <DateTimePicker
                    timeFormat={'HH:mm'}
                    onChange={date => setPublishedAt(date)}
                    value={publishedAt}
                    isValidDate={validatePublishedAd}
                  />
                </Col>
                <Col lg="4" style={{ marginBottom: 20 }}>
                  <label className='form-control-label'>
                    {'Start konsultacji '}
                    <b style={{ fontSize: 12, color: 'grey' }}>(Opcjonalnie)</b>
                  </label>
                  <DateTimePicker
                    timeFormat={'HH:mm'}
                    onChange={date => setBeginDate(date)}
                    value={beginDate}
                    isValidDate={validateBeginDate}
                  />
                </Col>
                <Col lg="4" style={{ marginBottom: 20 }}>
                  <label className='form-control-label'>
                    {'Koniec konsultacji '}
                    <b style={{ fontSize: 12, color: 'grey' }}>(Opcjonalnie)</b>
                  </label>
                  <DateTimePicker
                    timeFormat={'HH:mm'}
                    onChange={date => setEndDate(date)}
                    value={endDate}
                    isValidDate={validateEndDate}
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="12" style={{marginBottom: 30}}>
                  <label className='form-control-label'>Krótki opis</label>
                  <Editor
                    initialValue={form.short_description.value}
                    data={form.short_description.value}
                    init={editorSettings(200)}
                    onEditorChange={text => setShortDescriptionHandler(text)}
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="12" style={{marginBottom: 30}}>
                  <label className='form-control-label'>Dłuki opis</label>
                  <Editor
                    initialValue={form.long_description.value}
                    data={form.long_description.value}
                    init={editorSettings(300)}
                    onEditorChange={text => setLongDescriptionHandler(text)}
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="12" style={{marginBottom: 30}}>
                  <label className='form-control-label'>Ciało</label>
                  <Editor
                    initialValue={form.body.value}
                    data={form.body.value}
                    init={editorSettings(400)}
                    onEditorChange={text => setBodyHandler(text)}
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="6">
                  {setICategoriesHandler}
                </Col>
                <Col lg="6">
                  {setInstitutionHandler}
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <h6 className="heading-small text-muted mb-4">
              Ankieta
            </h6>
            <Row>
              <Col lg="12">
                <Survey
                  value={survey}
                  onChange={value => setSurvey(value)}
                  mode='edit'
                />
              </Col>
            </Row>
            <hr className="my-4" />
            <div className="pl-lg-4">
              <Row>
                <VotingMethods
                  value={votingMethod}
                  onChange={setVotingMethod}
                  type="new"
                />
              </Row>
            </div>
            <hr className="my-4" />
            <Button
              color={validation ? 'success' : 'danger'}
              onClick={createConsultation}
              disabled={!validation}
            >
              Dodaj
            </Button>
          </Form>
        </CardBody>
      </Card>
    </ContentWrapper>
  )
}

export default User
