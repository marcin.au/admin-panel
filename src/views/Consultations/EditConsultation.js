/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect } from 'react'
import ContentWrapper from '../../components/ContentWrapper'
import { Card, Row, Col, Button, CardBody, Form, FormGroup, Label, Spinner } from 'reactstrap'
import DateTimePicker from 'react-datetime'
import api from 'api'
import Loader from '../../components/Loader'
import Swal from 'sweetalert2'
import Input from '../../components/Input'
import { initialFormState, formPayload, parseErrors, getLocaDate } from '../../utils'
import validate from './Validate'
import Survey from '../../components/Survey'
import moment from 'moment'
import './Consultations.css'
import { currentInstitution } from '../../utils'
import { Editor } from '@tinymce/tinymce-react';
import VotingMethods from '../../components/VotingMethods/Container'

const EditConsultation = props => {
  const fields = [
    { name: 'long_description', required: false },
    // { name: 'published_at', required: true },
    { name: 'short_description', required: false },
    { name: 'title', required: true },
    { name: 'category_id', required: true, touched: true },
    { name: 'institution_id', required: true, touched: true },
    { name: 'body', required: true}
  ]

  const [loading, setLoading] = useState('hidden')
  const [form, setForm] = useState(initialFormState(fields))
  const [validation, setValidation] = useState(false)
  const [FieldsTouched, setFieldsTouched] = useState(false)
  const [categories, setCategories] = useState([])
  const [institution, setInstitution] = useState([])
  const [survey, setSurvey] = useState(null)
  const [surveyTouched, setSurveyTouched] = useState(false)
  const [beginDate, setBeginDate] = useState(null)
  const [endDate, setEndDate] = useState(null)
  const [publishedAt, setPublishedAt] = useState(moment().add(1, 'day'))
  const [votingMethod, setVotingMethod] = useState('open')
  const [initialPeselCount, setInitialPeselCount] = useState(0)

  const editorSettings = (height) => ({
    height: height,
    menubar: true,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table paste code help wordcount'
    ],
    toolbar:
      'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
  })

  useEffect(() => {
    const validateAll = () => {
      for (let field of fields) {
        if (form[field.name].errors) {
          return false
        }
      }
      return true
    }
    setValidation(validateAll())
  }, [fields, form])
  
  useEffect(() => {
    Object.keys(form).forEach(name => {
      const errors = validate(name, form[name].value, form)
      if (form[name].errors !== errors) {
        setForm(form => ({ ...form, [name]: { ...form[name], errors } }))
      }
    })
  }, [
    form.short_description.value,
    form.long_description.value,
    form.title.value,
    form.category_id,
    form.institution_id,
    form.body.value
  ])
  const validateEndDate = (current) => {
    return current.isAfter(beginDate)
  }

  const validateBeginDate = (current) => {
    return current.isAfter(moment())
  }

  const validatePublishedAd = (current) => {
    return current.isAfter(moment())
  }

  useEffect(() => {
    const fetchConsultation = async () => {
      setLoading('loading')
      const res = await api({ withInstitution: true }).get(`/consultations/${props.match.params.id}`)
      if (res.status === 200) {
        setLoading('success')
        const { data, included } = res.data
        console.log(data)
        setForm({
          long_description: { value: data.attributes.long_description ?? '<span />', touched: false, required: true, errors: false}, 
          short_description: { value: data.attributes.short_description ?? '<span />', touched: false, required: true, errors: false}, 
          title: { value: data.attributes.title, touched: false, required: true, errors: false},
          body: { value: data.attributes.body ?? '<span />', touched: false, required: false, errors: false}, 
          category_id: { value: included[0].id, touched: false, required: true, errors: false}, 
          institution_id: { value: data.relationships.institution.data.id, touched: false, required: true, errors: false}
        })
        setBeginDate(moment(data.attributes.begin_at))
        setEndDate(moment(data.attributes.end_at))
        setPublishedAt(moment(data.attributes.published_at))
        setSurvey(JSON.parse(data.attributes.survey) ?? {})
        setVotingMethod(data.attributes?.voting_method ?? 'open')
        setInitialPeselCount(data.attributes?.whitelist_items_counter ?? 0)
      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchConsultation()
  }, [])

  const updateConsultation = async () => {
    setLoading('loading')
    const payload = {
      consultation: {
        begin_at: beginDate ? (beginDate.format() === 'Invalid date' ? null : beginDate.format()) : null,
        end_at: endDate ? (endDate.format() === 'Invalid date' ? null : endDate.format()) : null,
        long_description: form.long_description.value,
        published_at: publishedAt.format(),
        short_description: form.short_description.value,
        title: form.title.value,
        body: form.body.value,
        category_id: form.category_id.value,
        institution_id: form.institution_id.value,
        survey: JSON.stringify(survey),
        voting_method: votingMethod,
        status: 'active'
      }
    }
    console.log(payload)
    const res = await api({ withInstitution: true }).put(`/consultations/${props.match.params.id}`, payload)
    if (res.status === 200) {
      setLoading('success')
      Swal.fire({
        title: 'Konsultacja zaktualizowana',
        icon: 'success',
        timer: 1000,
        showConfirmButton: false,
        timerProgressBar: true,
      })
      props.history.push(`/${currentInstitution()}/admin/consultations`)
    } else {
      console.log(res.data.data)
      setLoading('error')
      Swal.fire({
        title: 'Wystąpiły błedy',
        html: parseErrors(res.data.data.errors),
        icon: 'error'
      })
    }
  }

  const setShortDescriptionHandler = (text) => {
    if (!FieldsTouched) {
      setFieldsTouched(true)
    }
    setForm({...form, 'short_description': {value: text, touched: true}})
  }

  const setLongDescriptionHandler = (text) => {
    if (!FieldsTouched) {
      setFieldsTouched(true)
    }
    setForm({...form, 'long_description': {value: text, touched: true}})
  }

  const setBodyHandler = (text) => {
    if (!FieldsTouched) {
      setFieldsTouched(true)
    }
    setForm({...form, 'body': {value: text, touched: true}})
  }

  const updateForm = (event) => {
  
    const { name, value } = event.target;

    if (!FieldsTouched) {
      setFieldsTouched(true)
    }

    if (!form[name].touched) {
      setForm(form => ({ ...form, [name]: { ...form[name], touched: true } }))
    }

    if (form[name].value !== value) {
      setForm(form => ({ ...form, [name]: { ...form[name], value } }))
    }
  }

  useEffect(() => {
    const fetchCategories = async () => {
      setLoading('loading')
      const res = await api().get(`/categories`)
      if (res.status === 200) {
        setLoading('success')
        const data = res.data.data
        setCategories(data)

      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchCategories()
  }, []) 

  useEffect(() => {
    const fetchIntitution = async () => {
      setLoading('loading')
      const res = await api().get(`/institutions`)
      if (res.status === 200) {
        setLoading('success')
        const data = res.data.data
        setInstitution(data)
      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchIntitution()
  }, []) 

  const setInstitutionHandler = (
    <Input type="select" title="Gmina" name='institution_id' onChange={updateForm} data={form.institution_id}>
      {institution.map(item => (<option key={item.id} value={item.id.toString()}>{item.attributes.name}</option>))}
    </Input>
  )


  const setICategoriesHandler = (
    <Input type="select" title="Kategoria" name='category_id' onChange={updateForm} data={form.category_id}>
      {categories.map(item => (<option key={item.id} value={item.id.toString()}>{item.attributes.name}</option>))}
    </Input>
  )

  const loader = (
    <span style={styles.loader}>
      <Spinner color="primary" size="lg"/>
    </span>
  )

  const editorShortDescription = (
    <Col lg="12" style={{marginBottom: 30}}>
      <label className='form-control-label'>Krótki opis</label>
      <Editor
        initialValue={form.short_description.value}
        data={form.short_description.value}
        init={editorSettings(200)}
        onEditorChange={text => setShortDescriptionHandler(text)}
      />
    </Col>
  )

  const editorLongDescription = (
  <Col lg="12" style={{marginBottom: 30}}>
    <label className='form-control-label'>Dłuki opis</label>
    <Editor
      initialValue={form.long_description.value}
      data={form.long_description.value}
      init={editorSettings(300)}
      onEditorChange={text => setLongDescriptionHandler(text)}
    />
  </Col>
)

  const editorBody = (
    <Col lg="12" style={{marginBottom: 30}}>
      <label className='form-control-label'>Ciało</label>
      <Editor
        initialValue={form.body.value}
        data={form.body.value}
        init={editorSettings(400)}
        onEditorChange={text => setBodyHandler(text)}
      />
    </Col>
  )


  return (
    <ContentWrapper title="Edytuj Konsultację">
      <Loader status={loading} />
      <Card className="bg-secondary shadow">
        <CardBody>
          <Form>
            <h6 className="heading-small text-muted mb-4">
              Informacje o konsultacji
            </h6>
            <div className="pl-lg-4">
            <Row>
                <Col lg="12">
                  <Input
                    title="Tytuł"
                    placeholder="Tytuł"
                    data={form.title}
                    name='title'
                    onChange={updateForm}
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="4" style={{ marginBottom: 20 }}>
                  <label className='form-control-label'>Data publikacji</label>
                  <DateTimePicker
                    timeFormat={'HH:mm'}
                    onChange={date => {setPublishedAt(date); setSurveyTouched(true)}}
                    value={publishedAt}
                    isValidDate={validatePublishedAd}
                  />
                </Col>
                <Col lg="4" style={{ marginBottom: 20 }}>
                  <label className='form-control-label'>
                    {'Start konsultacji '}
                    <b style={{ fontSize: 12, color: 'grey' }}>(Opcjonalnie)</b>
                  </label>
                  <DateTimePicker
                    timeFormat={'HH:mm'}
                    onChange={date => {setBeginDate(date); setSurveyTouched(true)}}
                    value={beginDate}
                    isValidDate={validateBeginDate}
                  />
                </Col>
                <Col lg="4" style={{ marginBottom: 20 }}>
                  <label className='form-control-label'>
                    {'Koniec konsultacji '}
                    <b style={{ fontSize: 12, color: 'grey' }}>(Opcjonalnie)</b>
                  </label>
                  <DateTimePicker
                    timeFormat={'HH:mm'}
                    onChange={date => {setEndDate(date); setSurveyTouched(true)}}
                    value={endDate}
                    isValidDate={validateEndDate}
                  />
                </Col>
              </Row>
              <Row>
                {form.short_description.value || form.short_description.touched ? editorShortDescription : loader}
              </Row>
              <Row>
                {form.long_description.value || form.long_description.touched ? editorLongDescription : loader}
              </Row>
              <Row>
                {form.body.value || form.body.touched ? editorBody : loader}
              </Row>
              <Row>
                <Col lg="6">
                  {setICategoriesHandler}
                </Col>
                <Col lg="6">
                  {setInstitutionHandler}
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <h6 className="heading-small text-muted mb-4">
              Ankieta
            </h6>
            <Row>
              <Col lg="12">
                <Survey
                  value={survey}
                  onChange={value => { setSurvey(value); setSurveyTouched(true) }}
                  mode='edit'
                />
              </Col>
            </Row>
            <hr className="my-4" />
            <div className="pl-lg-4">
              <Row>
                <VotingMethods
                  value={votingMethod}
                  onChange={(value) => setVotingMethod(value) || setFieldsTouched(true)}
                  consultationId={props.match.params?.id}
                  type="edit"
                  initialCount={initialPeselCount}
                />
              </Row>
            </div>
            <hr className="my-4" />
            <Button
              color={validation && (FieldsTouched || surveyTouched) ? 'success' : 'danger'}
              onClick={updateConsultation}
              disabled={!validation || (!FieldsTouched && !surveyTouched)}
            >
              Aktualizuj
            </Button>
          </Form>
        </CardBody>
      </Card>
    </ContentWrapper>
  )
}

const styles = {
  loader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: 400
  }
}

export default EditConsultation
