export default (name, value, form) => {
  let errors = []
  if (!form[name].touched) return

  switch (name) {
    case 'title':
      if (value === '') {
        errors.push('Tytuł jest pusty')
      }
      break   
    // case 'short_description':
    //   if (value === '') {
    //     errors.push('Opis jest pusty')
    //   } else if (value.length < 10) {
    //     errors.push("Opis jest za krótki")
    //   }
    //   break
    // case 'long_description':
    //   if (value === '') {
    //     errors.push('Opis jest pusty')
    //   } else if (value.length < 50) {
    //     errors.push("Opis jest za krótki")
    //   }
      break
    default:
      break
  }

  return errors.length === 0 ? false : errors
}