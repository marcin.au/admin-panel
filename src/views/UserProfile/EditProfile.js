/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect } from 'react'
import ContentWrapper from '../../components/ContentWrapper'
import { Card, Row, Col, Button, CardBody, Form } from 'reactstrap'
import api from 'api'
import Loader from '../../components/Loader'
import Swal from 'sweetalert2'
import Input from '../../components/Input'
import { initialFormState, formPayload, currentInstitution } from '../../utils'
import validate from './Validate'
import {useDispatch, useSelector} from 'react-redux'
import { updateProfile } from 'redux/actionCreators/auth'

const EditProfile = props => {

    const userId = useSelector(store => store.auth.currentUser.id)

  const fields = [
    { name: 'email', required: true },
    { name: 'password', required: true },
    { name: 'password_confirmation', required: true },
    { name: 'first_name', required: true },
    { name: 'last_name', required: true },
    { name: 'role', required: true},
    { name: 'pesel', required: true}
  ]

  const [loading, setLoading] = useState('hidden')
  const [form, setForm] = useState(initialFormState(fields))
  const [validation, setValidation] = useState(false)
  const [FieldsTouched, setFieldsTouched] = useState(false)
  const [userPreviousEmail, setUserPreviousEmail] = useState('')

  

  const dispatch = useDispatch()

  useEffect(() => {
    const fetchUser = async () => {
    const user = await props.location.state.user
    console.log(user)
    for (let name in form) {
        if (user[name]) {
          setForm(form => ({...form, [name]: {...form[name], value: user[name]}}))
        }
      }
    }
  fetchUser()
}, [])


  useEffect(() => {
    const validateAll = () => {
      for (let field of fields) {
        if (form[field.name].errors) {
          return false
        }
      }
      return true
    }
    setValidation(validateAll())
  }, [fields, form])
  
  useEffect(() => {
    const validateEmailAvailability = async () => {
      if (!form.email.touched || userPreviousEmail === form.email.value) return
      if (validate('email', form.email.value, form)) return
      const payload = { user: { email: form.email.value } }
      const res = await api({ withInstitution: true }).post('/users', payload)
      const error = res.data.data.errors[0]
      if (error.detail === 'has already been taken' && error.source.pointer === '/data/attributes/email') {
        setForm(form => ({ ...form, email: { ...form.email, errors: ['Email jest zajęty'] } }))
      }
    }
    validateEmailAvailability()
  }, [form.email.value])

  useEffect(() => {
    Object.keys(form).forEach(name => {
      const errors = validate(name, form[name].value, form)
      if (form[name].errors !== errors) {
        setForm(form => ({ ...form, [name]: { ...form[name], errors } }))
      }
    })
  }, [
    form.email.value,
    form.password.value,
    form.password_confirmation.value,
    form.first_name.value,
    form.last_name.value,
    form.role.value,
    form.pesel.value
  ])


  const updateUser = async () => {
    console.log(form)
    setLoading('loading')
    console.log(formPayload(form))
    try{
    await dispatch(updateProfile(userId, formPayload(form)))
    } catch(error){
        setLoading('error')
          Swal.fire({
            title: 'Wystąpiły błedy',
            icon: 'error'
          }) 
        } finally {
            setLoading('success')
              Swal.fire({
                title: 'Użytkownik zaaktualizowany',
                icon: 'success',
                timer: 1000,
                showConfirmButton: false,
                timerProgressBar: true,
              })
              props.history.push(`/${currentInstitution()}/admin/user_profile`)
        }
  }

  const updateForm = (event) => {
    const { name, value } = event.target;

    if (!FieldsTouched) {
      setFieldsTouched(true)
    }

    if (!form[name].touched) {
      setForm(form => ({ ...form, [name]: { ...form[name], touched: true } }))
    }

    if (form[name].value !== value) {
      setForm(form => ({ ...form, [name]: { ...form[name], value } }))
    }
  }


  return (
    <ContentWrapper title="Edytuj profil">
      <Loader status={loading} />
      <Card className="bg-secondary shadow">
        <CardBody>
          <Form>
            <h6 className="heading-small text-muted mb-4">
              Podstawowe informacje
            </h6>
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  <Input
                    title="Email"
                    placeholder="Email"
                    data={form.email}
                    name='email'
                    onChange={updateForm}
                    type="email"
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Hasło"
                    placeholder="Hasło"
                    data={form.password}
                    name='password'
                    onChange={updateForm}
                    type="password"
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="6">
                  <Input
                    title="Rola"
                    placeholder="Rola"
                    data={form.role}
                    name='role'
                    onChange={updateForm}
                    type="text"
                    disabled
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Potwierdź hasło"
                    placeholder="Hasło"
                    data={form.password_confirmation}
                    name='password_confirmation'
                    onChange={updateForm}
                    type="password"
                  />
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <h6 className="heading-small text-muted mb-4">
              Dodatkowe informacje
            </h6>
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  <Input
                    title="Imię"
                    placeholder="Imię"
                    data={form.first_name}
                    name='first_name'
                    onChange={updateForm}
                    type="text"
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Nazwisko"
                    placeholder="Nazwisko"
                    data={form.last_name}
                    name='last_name'
                    onChange={updateForm}
                    type="text"
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="6">
                  <Input
                    title="Pesel"
                    placeholder="Pesel"
                    data={form.pesel}
                    name='pesel'
                    onChange={updateForm}
                    type="number"
                  />
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <Button
              color={validation && FieldsTouched ? 'success' : 'danger'}
              onClick={updateUser}
              disabled={!validation || !FieldsTouched}
            >
              Aktualizuj
            </Button>
          </Form>
        </CardBody>
      </Card>
    </ContentWrapper>
  )
}

export default EditProfile
