import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col
} from "reactstrap";
// core components
import UserHeader from "components/Headers/UserHeader.js";
import {useDispatch, useSelector} from 'react-redux'
import { currentInstitution } from '../../utils'

const Profile = props => {
    const user = useSelector(store => store.auth.userData)
    return (
      <>
        <UserHeader onClick={() => props.history.push({pathname: `/${currentInstitution()}/admin/user_profile/edit`, state: {user: user}})}/>
        {/* Page content */}
        <Container className="mt--7" fluid>
          <Row>
            <Col className="order-xl-1" xl="12">
              <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                  <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">Moje konto</h3>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Form>
                    <h6 className="heading-small text-muted mb-4">
                      Podstawowe informacje o użytkowniku
                    </h6>
                    <div className="pl-lg-4">
                    <Row>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-email"
                            >
                              Adres Email
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-email"
                              value={user.email}
                              disabled
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-role"
                            >
                              Rola
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-role"
                              value={user.role}
                              type="text"
                              disabled
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                    </div>
                    <hr className="my-4" />
                    <h6 className="heading-small text-muted mb-4">
                      Dodatkowe informacje o użytkowniku
                    </h6>
                    <div className="pl-lg-4">
                      <Row>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-first-name"
                            >
                              Imie
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-first-name"
                              value={user.first_name}
                              type="text"
                              disabled
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="id=input-last-name"
                            >
                              Nazwisko
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-last-name"
                              value={user.last_name}
                              type="text"
                              disabled
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="id=input-last-name"
                            >
                              Pesel
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-pesel"
                              value={user.pesel}
                              disabled
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                    </div>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  }

export default Profile;
