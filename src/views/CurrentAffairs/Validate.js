export default (name, value, form) => {
  let errors = []
  if (!form[name].touched) return

  switch (name) {
    case 'title':
      if (value === '') {
        errors.push('Tytuł jest pusty')
      }
      break
    case 'intro':
      if (value === '') {
        errors.push('Intro jest puste')
      }
      break
    case 'body':
      if (value === '') {
        errors.push('Ogłoszenie jest puste')
      }
      break
    default:
      break
  }

  return errors.length === 0 ? false : errors
}