import React, { useState, useEffect } from 'react'
import ContentWrapper from '../../components/ContentWrapper'
import { Card, Row, Col, Button, CardBody, Form } from 'reactstrap'
import api from 'api'
import Loader from '../../components/Loader'
import Swal from 'sweetalert2'
import Input from '../../components/Input'
import moment from 'moment'
import { initialFormState, formPayload, parseErrors, currentInstitution } from '../../utils'
import validate from './Validate'
import DateTimePicker from 'react-datetime'
import { Editor } from '@tinymce/tinymce-react';

const NewCurrentAffairs = props => {

  const fields = [
    { name: 'title', required: true },
    { name: 'intro', required: true },
    { name: 'body', required: true}
  ]

  const [loading, setLoading] = useState('hidden')
  const [form, setForm] = useState(initialFormState(fields))
  const [validation, setValidation] = useState(false)
  const [publishedAt, setPublishedAt] = useState(moment().add(1, 'month'))
  const [body, setBody] = useState('')

  const editorSettings = {
    height: 500,
    menubar: true,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table paste code help wordcount'
    ],
    toolbar:
      'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
  }

  useEffect(() => {
    const validateAll = () => {
      for (let field of fields) {
        if ((form[field.name].errors) || (field.required && !form[field.name].touched)) {
          return false
        }
      }
      return true
    }
    setValidation(validateAll())
  }, [fields, form])

  useEffect(() => {
    Object.keys(form).forEach(name => {
      const errors = validate(name, form[name].value, form)
      if (form[name].errors !== errors) {
        setForm(form => ({ ...form, [name]: { ...form[name], errors } }))
      }
    })
  }, [
    form.title.value,
    form.intro.value,
    form.body.value
  ])

  const createAffair = async () => {
    setLoading('loading')
    const res = await api({ withInstitution: true }).post('/current_affairs', { current_affair: {
      title: form.title.value,
      intro: form.intro.value,
      body: form.body.value,
      published_at: publishedAt.format(),
    }})
    if (res.status === 201) {
      setLoading('success')
      Swal.fire({
        title: 'Aktulaność została utworzony',
        icon: 'success',
        timer: 1000,
        showConfirmButton: false,
        timerProgressBar: true,
      })
      props.history.push(`/${currentInstitution()}/admin/current_affairs`)
    } else {
      setLoading('error')
      Swal.fire({
        title: 'Wystąpiły błedy',
        html: parseErrors(res.data.data.errors),
        icon: 'error'
      })
    }
  }

  const setBodyHandler = (text) => {
    setForm({...form, 'body': {value: text, touched: true}})
  }

  const updateForm = (event) => {
    const { name, value } = event.target;
    console.log(form)

    if (!form[name].touched) {
      setForm(form => ({ ...form, [name]: { ...form[name], touched: true } }))
    }

    if (form[name].value !== value) {
      setForm(form => ({ ...form, [name]: { ...form[name], value } }))
    }
  }

  return (
    <ContentWrapper title="Nowa aktualność">
      <Loader status={loading} />
      <Card className="bg-secondary shadow">
        <CardBody>
          <Form>
            <h6 className="heading-small text-muted mb-4">
              Podstawowe informacje
            </h6>
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  <Input
                    title="Tytuł"
                    placeholder="Tite"
                    data={form.title}
                    name='title'
                    onChange={updateForm}
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Intro"
                    placeholder="Intro"
                    data={form.intro}
                    name='intro'
                    onChange={updateForm}
                  />
                </Col>
              </Row>
              <Row style={{ marginBottom: 15 }}>
                <Col lg="12">
                  <Editor
                    initialValue={form.body.value}
                    data={form.body.value}
                    init={editorSettings}
                    onEditorChange={text => setBodyHandler(text)}
                  />
                </Col>
              </Row>
              <Row>
              <Col lg="4" style={{ marginBottom: 20 }}>
                  <label className='form-control-label'>Data publikacji</label>
                  <DateTimePicker
                    timeFormat={'HH:mm'}
                    onChange={date => setPublishedAt(date)}
                    value={publishedAt}
                  />
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <Button
              color={validation ? 'success' : 'error'}
              onClick={createAffair}
              disabled={!validation}
            >
              Dodaj
            </Button>
          </Form>
        </CardBody>
      </Card>
    </ContentWrapper>
  )
}

export default NewCurrentAffairs
