/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect } from 'react'
import ContentWrapper from '../../components/ContentWrapper'
import { Card, Row, Col, Button, CardBody, Form , Spinner} from 'reactstrap'
import api from 'api'
import Loader from '../../components/Loader'
import Swal from 'sweetalert2'
import Input from '../../components/Input'
import { initialFormState, formPayload, idFromPath, parseErrors, currentInstitution } from '../../utils'
import validate from './Validate'
import moment from 'moment'
import DateTimePicker from 'react-datetime'
import { Editor } from '@tinymce/tinymce-react';


const EditCurrentAffairs = props => {

  const fields = [
    { name: 'title', required: true },
    { name: 'intro', required: true },
    { name: 'body', required: true}
  ]

  const [loading, setLoading] = useState('hidden')
  const [form, setForm] = useState(initialFormState(fields))
  const [validation, setValidation] = useState(false)
  const [publishedAt, setPublishedAt] = useState(moment().add(1, 'month'))
  const [FieldsTouched, setFieldsTouched] = useState(false)

  useEffect(() => {
      const fetchAffair = async () => {
      setLoading('loading')
      const res = await api({ withInstitution: true }).get(`/current_affairs/${idFromPath()}`)
      if (res.status === 200) {
        console.log()
        setLoading('success')
        const data = res.data.data.attributes
        setForm({
          title: { value: data.title, touched: false, required: true, errors: false}, 
          intro: { value: data.intro, touched: false, required: true, errors: false}, 
          body:  { value: data.body, touched: false, required: true, error: false}
        })
        setPublishedAt(moment(data.published_at))
      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchAffair()
  }, [])

  const editorSettings = {
    height: 500,
    menubar: true,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table paste code help wordcount'
    ],
    toolbar:
      'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
  }


  useEffect(() => {
    const validateAll = () => {
      for (let field of fields) {
        if (form[field.name].errors) {
          return false
        }
      }
      return true
    }
    setValidation(validateAll())
  }, [fields, form])
  
  useEffect(() => {
    Object.keys(form).forEach(name => {
      const errors = validate(name, form[name].value, form)
      if (form[name].errors !== errors) {
        setForm(form => ({ ...form, [name]: { ...form[name], errors } }))
      }
    })
  }, [
    form.title.value,
    form.intro.value,
    form.body.value
  ])

  const updateAffair = async () => {
    setLoading('loading')
    const res = await api({ withInstitution: true }).put(`/current_affairs/${idFromPath()}`, { current_affair: {
      title: form.title.value,
      intro: form.intro.value,
      body: form.body.value,
      published_at: publishedAt.format(),
    }})
    if (res.status === 200) {
      setLoading('success')
      Swal.fire({
        title: 'Użytkownik utworzony',
        icon: 'success',
        timer: 1000,
        showConfirmButton: false,
        timerProgressBar: true,
      })
      props.history.push(`/${currentInstitution()}/admin/current_affairs`)
    } else {
      console.log(res)
      setLoading('error')
      Swal.fire({
        title: 'Wystąpiły błedy',
        html: parseErrors(res.data.data.errors),
        icon: 'error'
      })
    }
  }

  const setBodyHandler = (text) => {
    if (!FieldsTouched) {
      setFieldsTouched(true)
    }
    setForm({...form, 'body': {value: text, touched: true}})
  }

  const updateForm = (event) => {
    const { name, value } = event.target;

    if (!FieldsTouched) {
      setFieldsTouched(true)
    }

    if (!form[name].touched) {
      setForm(form => ({ ...form, [name]: { ...form[name], touched: true } }))
    }

    if (form[name].value !== value) {
      setForm(form => ({ ...form, [name]: { ...form[name], value } }))
    }
  }

  const loader = (
    <span style={styles.loader}>
      <Spinner color="primary" size="lg"/>
    </span>
  )

  const editor = (
    <Editor
      initialValue={form.body.value}
      data={form.body.value}
      init={editorSettings}
      onEditorChange={text => setBodyHandler(text)}
    />
  )


  return (
    <ContentWrapper title="Edytuj aktulaność">
      <Loader status={loading} />
      <Card className="bg-secondary shadow">
        <CardBody>
          <Form>
            <h6 className="heading-small text-muted mb-4">
              Podstawowe informacje
            </h6>
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  <Input
                    title="Tytuł"
                    placeholder="Tite"
                    data={form.title}
                    name='title'
                    onChange={updateForm}
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Intro"
                    placeholder="Intro"
                    data={form.intro}
                    name='intro'
                    onChange={updateForm}
                  />
                </Col>
              </Row>
              <Row style={{ marginBottom: 15 }}>
                <Col lg="12">
                  {form.body.value ? editor : loader}
                </Col>
              </Row>
              <Row>
              <Col lg="4" style={{ marginBottom: 20 }}>
                  <label className='form-control-label'>Data publikacji</label>
                  <DateTimePicker
                    timeFormat={'HH:mm'}
                    onChange={date => setPublishedAt(date)}
                    value={publishedAt}
                  />
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <Button
              color={validation && FieldsTouched ? 'success' : 'danger'}
              onClick={updateAffair}
              disabled={!validation || !FieldsTouched}
            >
              Aktualizuj
            </Button>
          </Form>
        </CardBody>
      </Card>
    </ContentWrapper>
  )
}

const styles = {
  loader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: 400
  }
}

export default EditCurrentAffairs

