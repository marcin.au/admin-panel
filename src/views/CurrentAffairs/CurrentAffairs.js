import React, { useState } from "react"
import Actions from '../../components/Actions'
import Paginator from '../../components/Paginator'
import { withRouter, Link } from 'react-router-dom'
import ContentWrapper from '../../components/ContentWrapper'
import ContentTable from '../../components/ContentTable'
import * as requests from '../../requests'
import Colors from '../../assets/variables/Colors'
import { parseDate } from '../../utils'
import { StylesManager } from "survey-creator"
import { currentInstitution } from '../../utils'

const CurrentAffairs = props => {

  const [institutions, setInstitutions] = useState([])

  const showHandler = id => {
    props.history.push(`/${currentInstitution()}/admin/current_affairs/${id}`)
  }

  const editHandler = id => {
    props.history.push(`/${currentInstitution()}/admin/current_affairs/${id}/edit`)
  }

  const deleteHandler = id => {
    const callback = () => setInstitutions(institutions => institutions.filter(item => item.id !== id))
    requests.destroy(`institutions/${currentInstitution()}/current_affairs/${id}`, callback)
  }

  const getDate = (date) => {
    return date ? parseDate(date) : <span style={{color: Colors.orange}}>Brak daty</span>
  }

  const institutionsJSX = institutions.map(item => (
    <tr key={item.id}>
      <td><Link to={`/${currentInstitution()}/admin/current_affairs/${item.id}`}>{item.id}</Link></td>
      <td><Link to={`/${currentInstitution()}/admin/current_affairs/${item.id}`}>{item.attributes.title}</Link></td>
      <td>{getDate(item.attributes.published_at)}</td>
      <td>
        <Actions
          showHandler={() => showHandler(item.id)}
          deleteHandler={() => deleteHandler(item.id)}
          editHandler={() => editHandler(item.id)}
        />
      </td>
    </tr>
  ))

  const headers = 'Id|Tytuł|Data dodania|Akcje'.split('|')

  return (
    <ContentWrapper title='Aktualności'>
      <ContentTable headers={headers}>
        {institutionsJSX}
      </ContentTable>
      <Paginator
        assetName={`current_affairs`}
        setCollection={setInstitutions}
      />
    </ContentWrapper>
  )
}


const styles = {
  logo: {
    maxWidth: 50,
    marginRight: 15
  }
}

export default withRouter(CurrentAffairs)
