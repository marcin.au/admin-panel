import React, { useState, useEffect } from 'react'
import ContentWrapper from '../../components/ContentWrapper'
import { Card, Row, Col, Button, CardBody, Form } from 'reactstrap'
import api from 'api'
import Loader from '../../components/Loader'
import Swal from 'sweetalert2'
import Input from '../../components/Input'
import { initialFormState, idFromPath, parseDate, currentInstitution } from '../../utils'
import ReactHtmlParser from 'react-html-parser'

const ShowCurrentAffairs = props => {

  const fields = [
    { name: 'title', required: true },
    { name: 'intro', required: true },
    { name: 'body', required: true },
    { name: 'published_at', required: true },
  ]

  const [loading, setLoading] = useState('hidden')
  const [form, setForm] = useState(initialFormState(fields))

  useEffect(() => {
    const fetchAffair = async () => {
    setLoading('loading')
    const res = await api({ withInstitution: true }).get(`/current_affairs/${idFromPath()}`)
    if (res.status === 200) {
      setLoading('success')
      console.log(res.data.data)
      const data = res.data.data.attributes
      for (let name in form) {
        if (data[name]) {
          setForm(form => ({...form, [name]: {...form[name], value: data[name]}}))
         }
      }
    } else {
      setLoading('error')
      Swal.fire({
        title: 'Coś poszło nie tak',
        icon: 'error'
      })
    }
  }
  fetchAffair()
}, [])

  return (
    <ContentWrapper title="Aktualność">
    <Loader status={loading} />
    <Card className="bg-secondary shadow">
      <CardBody>
        <Form>
          <h6 className="heading-small text-muted mb-4">
            Podstawowe informacje
          </h6>
          <div className="pl-lg-4">
            <Row>
              <Col lg="6">
                <Input
                  title="Tytuł"
                  placeholder="Tite"
                  data={form.title}
                  name='title'
                  disabled
                />
              </Col>
              <Col lg="6">
                <Input
                  title="Intro"
                  placeholder="Intro"
                  data={form.intro}
                  name='intro'
                  disabled
                />
              </Col>
            </Row>
            <Row>
              <Col lg="12">
                <label className='form-control-label'>Ogłoszenie</label>
                <br/>
                <div style={{padding: 10, border: 'solid 1px #ccc', marginBottom: 10}}>
                  {ReactHtmlParser(form.body.value)}
                </div>
              </Col>
            </Row>
            <Row>
            <Col lg="4" style={{ marginBottom: 20 }}>
                <label className='form-control-label'>Data publikacji</label>
                <br/>
                {parseDate(form.published_at.value)}
              </Col>
            </Row>
          </div>
          <hr className="my-4" />
          <Button
              color='primary'
              onClick={() => props.history.push(`/${currentInstitution()}/admin/current_affairs/${idFromPath()}/edit`)}
            >
              Edytuj
            </Button>
        </Form>
      </CardBody>
    </Card>
  </ContentWrapper>
  )
}


export default ShowCurrentAffairs
