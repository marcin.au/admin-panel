
import React, { useState } from "react"
import Paginator from '../../components/Paginator'
import { withRouter, Link } from 'react-router-dom'
import ContentWrapper from '../../components/ContentWrapper'
import ContentTable from '../../components/ContentTable'
import { currentInstitution } from "utils"

const Propositions = props => {

  const [propositions, setPropositions] = useState([])

  const propositionsJSX = propositions.map(item => (
    <tr key={item.id}>
      <td><Link to={`/${currentInstitution()}/admin/propositions/${item.id}`}>{item.id}</Link></td>
      <td><Link to={`/${currentInstitution()}/admin/propositions/${item.id}`}>{item.attributes.name}</Link></td>
      <td>{item.attributes.email}</td>
      <td>{item.attributes.subject}</td>
    </tr>
  ))

  const headers = 'Id|Nazwa|Email|Temat'.split('|')

  return (
    <ContentWrapper title='Propozycje'>
      <ContentTable headers={headers}>
        {propositionsJSX}
      </ContentTable>
      <Paginator
        assetName='propositions'
        setCollection={setPropositions}
        hideButton
      />
    </ContentWrapper>
  )
}

export default withRouter(Propositions)
