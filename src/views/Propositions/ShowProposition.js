/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect } from 'react'
import ContentWrapper from '../../components/ContentWrapper'
import { Card, Row, Col, CardBody, Form } from 'reactstrap'
import api from 'api'
import Loader from '../../components/Loader'
import Swal from 'sweetalert2'
import Input from '../../components/Input'
import { initialFormState } from '../../utils'

const ShowProposition = props => {

  const fields = [
    { name: 'name', required: true },
    { name: 'email', required: true },
    { name: 'phone', required: true },
    { name: 'subject', required: true },
    { name: 'description', required: true}
  ]

  const [loading, setLoading] = useState('hidden')
  const [form, setForm] = useState(initialFormState(fields))

  useEffect(() => {
    const fetchProposition = async () => {
      setLoading('loading')
      const res = await api({ withInstitution: true }).get(`/propositions/${props.match.params.id}`)
      if (res.status === 200) {
        const { attributes } = res.data.data
        let newForm = initialFormState(fields)
        fields.forEach(field => {
          newForm[field.name] = { ...newForm[field.name], value: attributes[field.name] }
        })
        setForm(newForm)
        setLoading('success')
      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchProposition()
  }, [])

  return (
    <ContentWrapper title="Pokaż propozycję konsultacji">
      <Loader status={loading} />
      <Card className="bg-secondary shadow">
        <CardBody>
          <Form>
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  <Input
                    title="Nazwa"
                    placeholder="Nazwa"
                    data={form.name}
                    disabled
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="6">
                  <label className="form-control-label">
                    Temat
                  </label>
                  <br />
                  {form.subject.value}
                </Col>
              </Row>
              <br />
              <Row>
                <Col lg="6">
                  <label className="form-control-label">
                    Opis
                  </label>
                  <br />
                  {form.description.value}
                </Col>
              </Row>
              <br />
              <Row>
                <Col lg="6">
                  <Input
                    title="Email"
                    placeholder="Email"
                    data={form.email}
                    disabled
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Telefon"
                    placeholder="Telefon"
                    data={form.phone}
                    disabled
                  />
                </Col>
              </Row>
            </div>
          </Form>
        </CardBody>
      </Card>
    </ContentWrapper>
  )
}


export default ShowProposition
