import React, { useState } from "react";
import Loader from 'components/Loader';
import Swal from 'sweetalert2'
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Col
} from "reactstrap";
import { useDispatch } from "react-redux"
import { login } from '../redux/actionCreators/auth'
import { currentInstitution } from '../utils'

const Login = props => {
  const [loading, setLoading] = useState('hidden')
  const [password, setPassword] = useState('MediaPark!3')
  const [email, setEmail] = useState('admin@media-park.pl')
  const dispatch = useDispatch()

  const submitHandler = async () => {
    setLoading('loading')
    try {
      await dispatch(login(email, password))
      props.history.push(`/${currentInstitution()}/admin/institutions`)
      setLoading('success')
    }
    catch (err) {
      setLoading('error')
      Swal.fire({
        title: 'Dane logowania niepoprawne',
        icon: 'error',
        timer: 1000,
        showConfirmButton: false,
        timerProgressBar: true
      })
    }
  }

  return (
    <React.Fragment>
      <Loader status={loading}/>
      <Col lg="5" md="7">
        <Card className="bg-secondary shadow border-0">
          <CardBody className="px-lg-5 py-lg-5">
            <Form role="form">
              <FormGroup className="mb-3">
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-email-83" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="Email"
                    type="email"
                    autoComplete="new-email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                  />
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-lock-circle-open" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="Hasło"
                    type="password"
                    autoComplete="new-password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    onKeyDown={e => e.key === 'Enter' ? submitHandler() : null}
                  />
                </InputGroup>
              </FormGroup>
              <div className="text-center">
                <Button className="my-4" color="primary" type="button" onClick={submitHandler}>
                  Zaloguj
                </Button>
              </div>
            </Form>
          </CardBody>
        </Card>
      </Col>
    </React.Fragment>
  )
}

export default Login
