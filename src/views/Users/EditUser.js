/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect } from 'react'
import ContentWrapper from '../../components/ContentWrapper'
import { Card, Row, Col, Button, CardBody, Form } from 'reactstrap'
import api from 'api'
import Loader from '../../components/Loader'
import Swal from 'sweetalert2'
import Input from '../../components/Input'
import { initialFormState, formPayload, parseErrors, idFromPath, currentInstitution } from '../../utils'
import validate from './Validate'

const User = props => {

  const fields = [
    { name: 'email', required: true },
    { name: 'password', required: true },
    { name: 'password_confirmation', required: true },
    { name: 'first_name', required: true },
    { name: 'last_name', required: true },
    { name: 'pesel', required: false },
    { name: 'role', required: true},
    { name: 'institution_id', required: true},
  ]

  const [loading, setLoading] = useState('hidden')
  const [form, setForm] = useState(initialFormState(fields))
  const [validation, setValidation] = useState(false)
  const [FieldsTouched, setFieldsTouched] = useState(false)
  const [userPreviousEmail, setUserPreviousEmail] = useState('')
  const [institution, setInstitution] = useState([])

  useEffect(() => {
    const validateAll = () => {
      for (let field of fields) {
        if (form[field.name].errors) {
          return false
        }
      }
      return true
    }
    setValidation(validateAll())
  }, [fields, form])
  
  useEffect(() => {
    const validateEmailAvailability = async () => {
      if (!form.email.touched || userPreviousEmail === form.email.value) return
      if (validate('email', form.email.value, form)) return
      const payload = { user: { email: form.email.value } }
      const res = await api({ withInstitution: true }).post('/users', payload)
      const error = res.data.data.errors[0]
      if (error.detail === 'has already been taken' && error.source.pointer === '/data/attributes/email') {
        setForm(form => ({ ...form, email: { ...form.email, errors: ['Email jest zajęty'] } }))
      }
    }
    validateEmailAvailability()
  }, [form.email.value])

  useEffect(() => {
    Object.keys(form).forEach(name => {
      const errors = validate(name, form[name].value, form)
      if (form[name].errors !== errors) {
        setForm(form => ({ ...form, [name]: { ...form[name], errors } }))
      }
    })
  }, [
    form.email.value,
    form.password.value,
    form.password_confirmation.value,
    form.first_name.value,
    form.last_name.value,
    form.pesel.value,
    form.role.value,
    form.institution_id
  ])

  useEffect(() => {
    const fetchUser = async () => {
      setLoading('loading')
      const res = await api({ withInstitution: true }).get(`/users/${idFromPath()}`)
      if (res.status === 200) {
        setLoading('success')
        const data = res.data.data.attributes
        setUserPreviousEmail(data.email)
        for (let name in form) {
          if (data[name]) {
            setForm(form => ({...form, [name]: {...form[name], value: data[name]}}))
          }
        }
      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchUser()
  }, [])

  const updateUser = async () => {
    setLoading('loading')
    const res = await api({ withInstitution: true }).put(`/users/${idFromPath()}`, { user: formPayload(form) })
    if (res.status === 200) {
      setLoading('success')
      Swal.fire({
        title: 'Użytkownik zaaktualizowany',
        icon: 'success',
        timer: 1000,
        showConfirmButton: false,
        timerProgressBar: true,
      })
      props.history.push(`/${currentInstitution()}/admin/users`)
    } else {
      console.log(res.data.data)
      setLoading('error')
      Swal.fire({
        title: 'Wystąpiły błedy',
        html: parseErrors(res.data.data.errors),
        icon: 'error'
      })
    }
  }

  const updateForm = (event) => {
    const { name, value } = event.target;

    if (!FieldsTouched) {
      setFieldsTouched(true)
    }

    if (!form[name].touched) {
      setForm(form => ({ ...form, [name]: { ...form[name], touched: true } }))
    }

    if (form[name].value !== value) {
      setForm(form => ({ ...form, [name]: { ...form[name], value } }))
    }
  }

  useEffect(() => {
    const fetchIntitution = async () => {
      setLoading('loading')
      const res = await api().get(`/institutions`)
      if (res.status === 200) {
        setLoading('success')
        const data = res.data.data
        console.log(data)
        setInstitution(data)
      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchIntitution()
  }, []) 


  const setInstitutionHandler = (
      <Col lg="12">
      <Input type="select" title="Gmina" defaultValue="Wybierz role..." name='institution_id' onChange={updateForm} data={form.institution_id}>
        {institution.map(item => (<option value={item.id} key={item.id}>{item.attributes.name}</option>))}
      </Input>
      </Col>
  )

  return (
    <ContentWrapper title="Edytuj użytkownika">
      <Loader status={loading} />
      <Card className="bg-secondary shadow">
        <CardBody>
          <Form>
            <h6 className="heading-small text-muted mb-4">
              Podstawowe informacje
            </h6>
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  <Input
                    title="Email"
                    placeholder="Email"
                    data={form.email}
                    name='email'
                    onChange={updateForm}
                    type="email"
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Hasło"
                    placeholder="Hasło"
                    data={form.password}
                    name='password'
                    onChange={updateForm}
                    type="password"
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="6"></Col>
                <Col lg="6">
                  <Input
                    title="Potwierdź hasło"
                    placeholder="Hasło"
                    data={form.password_confirmation}
                    name='password_confirmation'
                    onChange={updateForm}
                    type="password"
                  />
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <h6 className="heading-small text-muted mb-4">
              Dodatkowe informacje
            </h6>
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  <Input
                    title="Imię"
                    placeholder="Imię"
                    data={form.first_name}
                    name='first_name'
                    onChange={updateForm}
                    type="text"
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Nazwisko"
                    placeholder="Nazwisko"
                    data={form.last_name}
                    name='last_name'
                    onChange={updateForm}
                    type="text"
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="6">
                  <Input
                    title="PESEL"
                    placeholder="PESEL"
                    data={form.pesel}
                    name='pesel'
                    onChange={updateForm}
                    type="number"
                  />
                </Col>
                <Col lg="6">
                <Input type="select" title="Typ Uytkownika" defaultValue="Wybierz role..." name='role' placeholder="role" onChange={updateForm} data={form.role}>
                  <option value={"superadmin"}>Super admin</option>
                  <option value={"admin"}>Admin</option>
                  <option value={"viewer"}>Viewer</option>
                </Input>
                </Col>
              </Row>
              <Row>
                  {form.role.value !== 'superadmin' ? setInstitutionHandler : null}
              </Row>
            </div>
            <hr className="my-4" />
            <Button
              color={validation && FieldsTouched ? 'success' : 'danger'}
              onClick={updateUser}
              disabled={!validation || !FieldsTouched}
            >
              Aktualizuj
            </Button>
          </Form>
        </CardBody>
      </Card>
    </ContentWrapper>
  )
}

export default User
