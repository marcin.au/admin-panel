
import React, { useState } from "react"
import Actions from '../../components/Actions'
import Paginator from '../../components/Paginator'
import { withRouter, Link } from 'react-router-dom'
import ContentWrapper from '../../components/ContentWrapper'
import ContentTable from '../../components/ContentTable'
import * as requests from '../../requests'
import Colors from '../../assets/variables/Colors'
import { parseDate, currentInstitution } from '../../utils'

const Users = props => {

  const [users, setUsers] = useState([])

  const showHandler = id => {
    props.history.push(`/${currentInstitution()}/admin/users/${id}`)
  }

  const editHandler = id => {
    props.history.push(`/${currentInstitution()}/admin/users/${id}/edit`)
  }

  const deleteHandler = id => {
    const callback = () => setUsers(users => users.filter(item => item.id !== id))
    requests.destroy(`institutions/${currentInstitution()}/users/${id}`, callback)
  }

  const active = <b style={{ color: Colors.green }}>Aktywny</b>
  const inactive = <b style={{ color: Colors.red }}>Nie aktywny</b>

  const parseRole = (role) => {
    if(role === 'superadmin') return 'Super Admin'
    if(role === 'admin') return 'Admin'
    if(role === 'viewer') return 'Uytkownik'
  }

  const getDate = (date) => {
    return date ? parseDate(date) : <span style={{color: Colors.grey}}>Brak pierwszego logowania</span>
  }

  const usersJSX = users.map(item => (
    <tr key={item.id}>
      <td>{item.id}</td>
      <td><Link to={`/${currentInstitution()}/admin/users/${item.id}`}>{item.attributes.email}</Link></td>
      <td>{item.attributes.active ? active : inactive}</td>
      <td>{parseRole(item.attributes.role)}</td>
      <td>{getDate(item.attributes.last_sign_in_at)}</td>
      <td>
        <Actions
          showHandler={() => showHandler(item.id)}
          deleteHandler={() => deleteHandler(item.id)}
          editHandler={() => editHandler(item.id)}
        />
      </td>
    </tr>
  ))

  const headers = 'Id|Email|Aktywny|Rola|Ostatnio zalogowany|Akcje'.split('|')

  return (
    <ContentWrapper title='Użytkownicy'>
      <ContentTable headers={headers}>
        {usersJSX}
      </ContentTable>
      <Paginator
        assetName='users'
        setCollection={setUsers}
      />
    </ContentWrapper>
  )
}

export default withRouter(Users)
