export default (name, value, form) => {
  let errors = []
  if (!form[name].touched) return

  switch (name) {
    case 'email':
      const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      if (value === '') {
        errors.push('Email jest pusty')
      }
      else if (!regex.test(String(value).toLocaleLowerCase())) {
        errors.push('Email jest nie poprawny')
      }
      break
    case 'password':
      if (value === '') {
        errors.push('Hasło jest puste')
      }
      else if (value.length < 6) {
        errors.push('Hasło jest za krótkie')
      }
      else if (value !== form.password_confirmation.value) {
        errors.push('Hasła nie są takie same')
      }
      break
    case 'password_confirmation':
      if (value === '') {
        errors.push('Potwierdzenie hasła jest puste')
      }
      else if (value.length < 6) {
        errors.push('Potwierdzenie hasła jest za krótkie')
      }
      else if (value !== form.password.value) {
        errors.push('Hasła nie są takie same')
      }
      break
    case 'first_name':
      if (value === '') {
        errors.push('Imię jest puste')
      }
      else if (value.length < 2) {
        errors.push('Imię jest za krótkie')
      }
      break
    case 'last_name':
      if (value === '') {
        errors.push('Nazwisko jest puste')
      }
      else if (value.length < 2) {
        errors.push('Nazwisko jest za krótkie')
      }
      break
    case 'pesel':
      if (value.length !== 11 && value !== '') {
        errors.push('PESEL musi składać się z 11 znaków')
      }
      break
    default:
      break
  }

  return errors.length === 0 ? false : errors
}