/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect } from 'react'
import ContentWrapper from '../../components/ContentWrapper'
import { Card, Row, Col, Button, CardBody, Form } from 'reactstrap'
import api from 'api'
import Loader from '../../components/Loader'
import Swal from 'sweetalert2'
import Input from '../../components/Input'
import { initialFormState, idFromPath, currentInstitution } from '../../utils'

const User = props => {

  const fields = [
    { name: 'email', required: true },
    { name: 'password', required: true },
    { name: 'password_confirmation', required: true },
    { name: 'first_name', required: true },
    { name: 'last_name', required: true },
    { name: 'pesel', required: false },
    { name: 'role', required: true },
  ]

  const [loading, setLoading] = useState('hidden')
  const [form, setForm] = useState(initialFormState(fields))

  useEffect(() => {
    const fetchUser = async () => {
      setLoading('loading')
      const res = await api({ withInstitution: true }).get(`/users/${idFromPath()}`)
      if (res.status === 200) {
        setLoading('success')
        const data = res.data.data.attributes
        for (let name in form) {
          setForm(form => ({...form, [name]: {...form[name], value: data[name]}}))
        }
      } else {
        setLoading('error')
        Swal.fire({
          title: 'Coś poszło nie tak',
          icon: 'error'
        })
      }
    }
    fetchUser()
  }, [])

  return (
    <ContentWrapper title="Pokaż Użytkownika">
      <Loader status={loading} />
      <Card className="bg-secondary shadow">
        <CardBody>
          <Form>
            <h6 className="heading-small text-muted mb-4">
              Podstawowe informacje
            </h6>
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  <Input
                    title="Email"
                    placeholder="Email"
                    data={form.email}
                    name='email'
                    type="email"
                    disabled
                  />
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <h6 className="heading-small text-muted mb-4">
              Dodatkowe informacje
            </h6>
            <div className="pl-lg-4">
              <Row>
                <Col lg="6">
                  <Input
                    title="Imię"
                    placeholder="Imię"
                    data={form.first_name}
                    name='first_name'
                    type="text"
                    disabled
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Nazwisko"
                    placeholder="Nazwisko"
                    data={form.last_name}
                    name='last_name'
                    type="text"
                    disabled
                  />
                </Col>
              </Row>
              <Row>
                <Col lg="6">
                  <Input
                    title="PESEL"
                    placeholder="PESEL"
                    data={form.pesel}
                    name='pesel'
                    type="number"
                    disabled
                  />
                </Col>
                <Col lg="6">
                  <Input
                    title="Typ użytkownika"
                    placeholder="Typ użytkownika"
                    data={form.role}
                    name='role'
                    disabled
                  />
                </Col>
              </Row>
            </div>
            <hr className="my-4" />
            <Button
              color='primary'
              onClick={() => props.history.push(`/${currentInstitution()}/admin/users/${idFromPath()}/edit`)}
            >
              Edytuj
            </Button>
          </Form>
        </CardBody>
      </Card>
    </ContentWrapper>
  )
}


export default User
