## Description
Admin panel for the consultations app
## Getting started
##### Install packages:
`npm install`
##### Fill environment variables
`cp .env.example .env` and edit if necessary
##### Start development server
`npm start`
## Dependencies
- node v13.13.0
